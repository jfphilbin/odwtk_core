Tag Generation

The programs and data files in this directory are used to generate various
libraries for dealing with DICOM Tags.

A Tag is a 32 bit number usually displayed in hexadecimal format.  A Tag is a number 
that uniquely identified a DICOM Data Element or Attribute.

This directory contains the following generators:

  1.  generate_tag_definitions.dart

      This programs generates a libary 'tag_defintions.dart' that contains a set
      of top level constant definitions in the form of 
      
        const keyword = tag; // comment
        
      where keyword is the keyword associated with tag in the Data Element table in 
      PS 3.6.
      
  2.  generate_tag_keyword_lookup_list.dart
  
      This program generates a list called 'tagKeywords', which when indexed by a Tag
      will return the [Keyword] associated with that tag as a [String].
      
  3.  generate_tag_name_lookup_list.dart
  
      This program generates a list called 'tagNames', which when indexed by a Tag
      will return the [Name] associated with that tag as a [String].
      
  4.  generate_tag_vr_lookup_list.dart
  
      This program generates a Uint8List called 'tagToVRIndex', which when indexed by a tag
      will return a Uint8 integer that can be used to index the [VRList] object 
      associated with that tag.
      
      Looking up a VR object using a Tag can be do with 
      
          tagToVR(int tag) {
            if (notValidTag(tag)) throw new Error;
            return VRList(tagToVRIndex(tag));
      
  5.  generate_tag_vm_lookup_list.dart
  
      This program generates a list called 'tagVMs', which when indexed by a tag
      will return the [VM] object associated with that tag.
      
  6.  generate_data_element_class.dart
  
      This program generates a constant class that contains DICOM Data Element
      Definitions.  Each data element contains a Tag, Keyword, Name, VR, VM, and
      isRetired.
 
 // this may not be generated     
  7.  generate_attribute_class.dart
  
      This program generates a class for instantiating DICOM attributes.  Each Attribute
      has a Tag, and a Value.  It is implemented using a Map.