// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

var unix = new Uri.file('/MICA/git/DCMiD/data_types/lib/data_types.dart');
//var windows = new Uri.file('C:\MICA\git\DCMiD\data_types\lib\data_types.dart');
var relative = new Uri.file('../lib/data_types.dart');

var wint = new Uri.file('C:/MICA/git/DCMiD/data_types/lib/data_types.dart', windows: true);
var winf = new Uri.file('C:/MICA/git/DCMiD/data_types/lib/data_types.dart', windows: false);

void main() {
  int i = 0x00080001;
  print(i);

  String s = tagToHexString(i);
  print(s);
  s = tagToHexString(i, DICOM: true);
  print(s);
}

String tagToHexString(i, {bool DICOM: false}) {
  String s = i.toRadixString(16);
  while (s.length < 8) s = "0" + s;
  if (DICOM) return "(" + s.substring(0, 4) + "," + s.substring(4,8) + ")";
  else return "0x" + s;
}


/*
void main() {
  print('+'.codeUnitAt(0));
  print('-'.codeUnitAt(0));


  var unixFilePath = unix.toFilePath();
  var resolved = unix.resolveUri(relative);
  var resolvedFilePath = resolved.toFilePath();

  // YYYYMMDDHHMMSS.FFFFFF&ZZXX


  var date = DateTime.parse("20100301");
  print(date);
  //var time = DateTime.parse("T120159");
  //print(time);
  var dateTime = DateTime.parse("20100301T120000");
  print("dateTime=$dateTime");
  var val = int.parse("-345");
  print(val);
  var dbl = double.parse("-345E34");
  print(dbl);

  print("unix = $unix");
  //print("unixFilePath = $unixFilePath");
  print("Resolved relative = $resolved");
  print("unix == resolved? ${unix == resolved}");
  print("relative = $resolvedFilePath unix = $unixFilePath: ${unixFilePath == resolvedFilePath}");
  //print("unix = $windows");
  print("wint = $wint");
  print("wint path = ${wint.toFilePath(windows: true)}");
  print("winf = $winf");

  print(Study);

  //print("$isInitialized");
  print("$IELevel.Study");
  print("$VM.VM_1");
  print("$IELevel.Study.name = $IELevel.Study.level");
  print("$Sex.FEMALE");

}
*/