// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

import 'package:unittest/unittest.dart';

import 'dart:typed_data';
import 'package:core/src/date/date_time_reader.dart';
import 'package:core/src/date/time.dart';

// Strings with valid HourMinuteSecond times
List<List> validHourMinuteSecondTimes
    = [["000000", true, "valid"],
       ["010101", true, "valid"],
       ["022029", true, "valid"],
       ["121212", true, "valid"],
       ["151515", true, "valid"],
       ["202020", true, "valid"],
       ["235959", true, "valid"],
       ["235950", true, "valid"]];

// String with valid HourMinute only times
List<List> validHourMinuteOnlyTimes
    = [["0000", true, "valid"],
       ["0101", true, "valid"],
       ["0220", true, "valid"],
       ["1212", true, "valid"],
       ["1515", true, "valid"],
       ["2020", true, "valid"],
       ["2359", true, "valid"],
       ["2359", true, "valid"]];

// String with valid Hours only times
List<List> validHourOnlyTimes
   = [["00", true, "valid"],
      ["01", true, "valid"],
      ["02", true, "valid"],
      ["12", true, "valid"],
      ["15", true, "valid"],
      ["20", true, "valid"],
      ["22", true, "valid"],
      ["23", true, "valid"]];

List<List> validHourMinuteSecondMicrosecondTimes
    = [["000000.123456", true, "valid"],
       ["010101.12345",  true, "valid"],
       ["022029.1234",   true, "valid"],
       ["121212.123",    true, "valid"],
       ["151515.12",     true, "valid"],
       ["202020.1",      true, "valid"],
       ["235959.987654", true, "valid"],
       ["235950.98765",  true, "valid"]];

List<List> validTimesWithTrailingSpaces
    = [["000000          ", true, "valid"],
       ["010101    ",       true, "valid"],
       ["0101    ",         true, "valid"],
       ["0220   ",          true, "valid"],
       ["15          ",     true, "valid"],
       ["20       ",        true, "valid"],
       ["000000.123456   ", true, "valid"],
       ["010101.12345  ",   true, "valid"],
       ["022029.1234 ",     true, "valid"],
       ["202020.1      ",   true, "valid"],
       ["235960.98765   ",  true, "valid"]];

List invalidTimesWithSpaces
    = [["19011 ",       false, "spaces in string"],
       ["1901 1",       false, "spaces in string"],
       ["190 01",       false, "spaces in string"],
       ["19 101",       false, "spaces in string"],
       ["1 0101",       false, "spaces in string"],
       [" 90101",       false, "spaces in string"],
       ["111111.  01",  false, "spaces in string"],
       ["123456.78  1", false, "spaces in string"]];

// Strings with bad time formats
List<List> invalidTimes
   = [["-10101",         false, "- sign not allowed"],
      ["241060",         false, "bad hour"],
      ["126059",         false, 'bad minute'],
      ["201462",         false, 'bad second'],
      ["2014-2",         false, 'bad second'],
      ["201-12",         false, 'bad minute'],
      ["20-111",         false, 'bad minute'],
      ["x23456",         false, 'bad hour'],
      ["1x3456",         false, 'bad hour'],
      ["12x456",         false, 'bad minute'],
      ["123x56",         false, 'bad minute'],
      ["1234x6",         false, 'bad second'],
      ["12345x",         false, 'bad second'],
      ["123456x",        false, 'bad decimal point'],
      ["123456x7",       false, 'bad decimal point'],
      ["123456.x",       false, 'bad fraction'],
      ["123456.x1",      false, 'bad fraction'],
      ["123456.1x2",     false, 'bad fraction'],
      ["123456.123x",    false, 'bad fraction'],
      ["121212.9999999", false, 'bad fraction - too long'],
      ["000000,+31",     false, 'bad fraction - no sign allowed']];

typedef dynamic Parser(String);
typedef dynamic LeapYearTester(String);

void main() {

  void timeParserTest(Parser parser, String name) {

    group('$name: Time Parser', () {

      test('Valid HourMinuteSecondTimes', () {
        for(List list in validHourMinuteSecondTimes) {
          String time = list[0];
          String reason = list[2];
          //print(Time);
         expect(parser(time), equals(time), reason: reason);
        }
      });

      test('Valid HourMinutes only times', () {
        for(List list in validHourMinuteOnlyTimes) {
          String time = list[0];
          String reason = list[2];
         expect(parser(time), equals(time), reason: reason);
        }
      });

      test('Valid Hour only times', () {
        for(List list in validHourOnlyTimes) {
          String time = list[0];
          String reason = list[2];
         expect(parser(time), equals(time), reason: reason);
        }
      });

      test('Invalid times', () {
        for(List list in invalidTimes) {
          String time = list[0];
          String reason = list[2];
          //print(time);
         expect((){parser(time);}, throws, reason: reason);
        }
      });

      test('Invalid times with Spaces', () {
        for(List list in invalidTimesWithSpaces) {
          String time = list[0];
          String reason = list[2];
          //print(time);
         expect((){parser(time);}, throws, reason: reason);
        }
      });

    });

  }

  String parseTimeString(String s) {
    DateTimeReader input = new DateTimeReader.fromString(s);
    Time time = input.readTime();
    return time.toString();

  }

  String parseTimeBytes(String s) {
    Uint8List bytes = new Uint8List.fromList(s.codeUnits);
    DateTimeReader input = new DateTimeReader(bytes);
    Time time = input.readTime();
    return time.toString();
  }

  timeParserTest(parseTimeString, "parseTimeString");
  timeParserTest(parseTimeBytes, "parseTimeBytes");

  // delete when debugged
/*
  for(List list in [["123456x", false, "bad"]]) {
    String time = list[0];
    String reason = list[2];
    print('string=$time');
    print(parseString(time));
  }
*/
}