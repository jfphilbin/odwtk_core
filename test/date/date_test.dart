// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

import 'package:unittest/unittest.dart';

import 'dart:typed_data';
import 'package:core/src/date/date.dart';
import 'package:core/src/date/date_time_reader.dart';
import 'package:core/src/date/date_time_utils.dart' as dtu;

// Strings with valid YearMonthDate dates
List<List> validYearMonthDateStrings
    = [[dtu.firstYear.toString(), true, "Minimum Date String as defined in Date"],
       [dtu.lastYear.toString(),  true, "Maximum Date String as defined in Date"],
       ["20140723", true, "valid"],
       ["19010101", true, "valid"],
       ["20001231", true, "valid"],
       ["20140723", true, "valid"]];

// String with valid YearMonth dates
List<List> validYearMonthOnlyDateStrings
    = [["201403", true, 'valid'],
       ["190101", true, 'valid'],
       ["200012", true, 'valid'],
       ["190002", true, 'valid'],
       ["200109", true, 'valid']];

// Strings with valid Year only
List<List> validYearOnlyDateStrings
    = [["2014", true, 'valid'],
       ["1901", true, 'valid'],
       ["2000", true, 'valid'],
       ["1900", true, 'valid'],
       ["2001", true, 'valid']];

List invalidDatesWithSpaces
    = [["1950011 ", false, "spaces in string"],
       ["195001 1", false, "spaces in string"],
       ["1950  01", false, "spaces in string"],
       ["1950 101", false, "spaces in string"],
       ["195 0101", false, "spaces in string"],
       ["19 00101", false, "spaces in string"],
       ["1 500101", false, "spaces in string"],
       [" 9500101", false, "spaces in string"]];

// Strings with bad date formats
List<List> invalidDateStrings
   = [["19000231",  false, "February doesn't have 31 days"],
      ["20010931",  false, "September doesn't have 31 days"],
      ["201407310", false, '9 characters'],
      ["2014073",   false, '7 characters'],
      ["20140",     false, '5 characters'],
      ["201",       false, '3 characters'],
      ["190101x0",  false, '"x" in day'],
      ["20001x31",  false, '"x" in month'],
      ["19x00031",  false, '"x" in year'],
      ["9999  31",  false, 'internal spaces']];

// Strings with bad date formats
List<List> leapYearStrings
  = [["1904",  true, 'valid leap year'],
     ["1996",  true, 'valid leap year'],
     ["1956",  true, 'valid leap year'],
     ["2004",  true, 'valid leap year'],
     ["1584",  true, 'valid Leap Year'],
     ["9996",  true, 'valid Leap Year']];

List<List> nonLeapYearStrings
  = [["1903",  true, 'invalid leap year'],
     ["1997",  true, 'invalid leap year'],
     ["1954",  true, 'invalid leap year'],
     ["2006",  true, 'invalid leap year'],
     ["1582",  true, 'invalid Leap Year'],
     ["9997",  true, 'invalid Leap Year']];

typedef dynamic Parser(String);
typedef dynamic LeapYearTester(String);

void main() {

  void leapYearsTest(LeapYearTester testLeapYear, String name) {

    group('$name: Leap Year Tests', () {

      test('Valid Leap Years', () {
        for(List list in leapYearStrings) {
          String date = list[0];
          String reason = list[2];
          //print(date);
         expect(testLeapYear(date), isTrue, reason: reason);
        }
      });

      test('Invalid Leap Years', () {
        for(List list in nonLeapYearStrings) {
          String date = list[0];
          String reason = list[2];
          //print(date);
         expect(testLeapYear(date), isFalse, reason: reason);
        }
      });

    });
  }

  void dateParserTest(Parser parser, String name) {

    group('$name: Date Parser', () {

      test('Valid YearMonthDay dates', () {
        for(List list in validYearMonthDateStrings) {
          String date = list[0];
          String reason = list[2];
          //print(date);
         expect(parser(date), equals(date), reason: reason);
        }
      });

      test('Valid YearMonth only dates', () {
        for(List list in validYearMonthOnlyDateStrings) {
          String date = list[0];
          String reason = list[2];
         expect(parser(date), equals(date), reason: reason);
        }
      });

      test('Valid Year only dates', () {
        for(List list in validYearOnlyDateStrings) {
          String date = list[0];
          String reason = list[2];
         expect(parser(date), equals(date), reason: reason);
        }
      });

      test('Invalid dates', () {
        for(List list in invalidDateStrings) {
          String date = list[0];
          String reason = list[2];
          //print(date);
         expect((){parser(date);}, throws, reason: reason);
        }
      });

      test('Invalid dates with Spaces', () {
        for(List list in invalidDatesWithSpaces) {
          String date = list[0];
          String reason = list[2];
          //print(date);
         expect((){parser(date);}, throws, reason: reason);
        }
      });

    });

  }

  bool testLeapYearString(String year) {
    DateTimeReader input = new DateTimeReader.fromString(year, 0, year.length);
    Date date = input.date;
    return date.isLeapYear;
  }

  bool testLeapYearBytes(String year) {
    Uint8List bytes = new Uint8List.fromList(year.codeUnits);
    DateTimeReader input = new DateTimeReader(bytes);
    Date date = input.date;
    return date.isLeapYear;
  }

  String parseDateString(String s) {
    DateTimeReader input = new DateTimeReader.fromString(s);
    return input.date.toString();
  }

  String parseDateBytes(String s) {
    Uint8List bytes = new Uint8List.fromList(s.codeUnits);
    DateTimeReader input = new DateTimeReader(bytes);
    Date date = input.date;
    return date.toString();
  }

  leapYearsTest(testLeapYearString, "LeapYearString");
  leapYearsTest(testLeapYearBytes, "LeapYearBytes");
  dateParserTest(parseDateString, "parseString");
  dateParserTest(parseDateBytes, "parseBytes");
/*
  // delete when debugged
  DcmDate.setYearRange(0000, 2000);
  print('1584=${DcmDate.isLeapYear(new DcmDate(1584))}');
  print('2001= ${new DcmDate(2000)}');

  for(List list in leapYearStrings) {
    String date = list[0];
    String reason = list[2];
    print('string=$date');
    print(testLeapYearString(date));
  }
*/
}