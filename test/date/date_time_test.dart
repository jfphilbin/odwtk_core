// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

import 'package:unittest/unittest.dart';

import 'dart:core' hide DateTime;
import 'dart:typed_data';
import 'package:core/src/date/date_time.dart';
import 'package:core/src/date/date_time_reader.dart';

// Strings with valid YearMonthDate dates without TimeZones
List<List> validDateTimesWithoutTimeZones
    = [//YYYY
       ["2014", true, 'valid'],
       ["1901", true, 'valid'],
       ["2000", true, 'valid'],
       ["1900", true, 'valid'],
       ["2001", true, 'valid'],

       //YYYYMM
       ["201403", true, 'valid'],
       ["190101", true, 'valid'],
       ["200012", true, 'valid'],
       ["190002", true, 'valid'],
       ["200109", true, 'valid'],

       //YYYYMMDD
       ["20140723", true, "valid"],
       ["19010101", true, "valid"],
       ["20001231", true, "valid"],
       ["20140723", true, "valid"],

       //YYYYMMDDHH
       ["2014072300", true, "valid"],
       ["1901010101", true, "valid"],
       ["2000123102", true, "valid"],
       ["2014072312", true, "valid"],
       ["1583030315", true, "valid"],
       ["1800060620", true, "valid"],
       ["1999090923", true, "valid"],
       ["2010111123", true, "valid"],
       ["2014072300", true, "valid"],
       ["2014072300", true, "valid"],

       //YYYYMMDDHHMM
       ["201407230000", true, "valid"],
       ["190101010101", true, "valid"],
       ["200012310220", true, "valid"],
       ["201407231212", true, "valid"],
       ["158303031515", true, "valid"],
       ["180006062020", true, "valid"],
       ["199909092359", true, "valid"],
       ["201011112359", true, "valid"],
       ["201407230000", true, "valid"],
       ["201407230000", true, "valid"],

       //YYYYMMDDHHMMSS
       ["20140723000000", true, "valid"],
       ["19010101010101", true, "valid"],
       ["20001231022029", true, "valid"],
       ["20140723121212", true, "valid"],
       ["15830303151515", true, "valid"],
       ["18000606202020", true, "valid"],
       ["19990909235959", true, "valid"],
       ["20101111235950", true, "valid"],
       ["20140723000000", true, "valid"],
       ["20140723000000", true, "valid"],

       //YYYYMMDDHHMMSS.FFFFFF
       ["20140723000000.0", true, "valid"],
       ["20140723000000.01", true, "valid"],
       ["19010101010101.012", true, "valid"],
       ["20001231022029.0123", true, "valid"],
       ["20140723121212.01234", true, "valid"],
       ["15830303151515.012345", true, "valid"],
       ["18000606202020.543210", true, "valid"],
       ["19990909235959.432100", true, "valid"],
       ["20101111235950.321000", true, "valid"],
       ["20140723000000.21000  ", true, "valid"],
       ["20140723000000.1000   ", true, "valid"],

       //YYYYMMDDHHMMSS
       ["19010101010101", true, "valid"],
       ["20001231022029 ", true, "valid"],
       ["20140723121212  ", true, "valid"],
       ["15830303151515   ", true, "valid"],
       ["18000606202020  ", true, "valid"],
       ["19990909235959  ", true, "valid"],
       ["20101111235950            ", true, "valid"]];

// Strings with valid YearMonthDate dates
List<List> validDateTimesWithTimeZones
 = [//YYYY
    ["2014", true, 'valid'],
    ["1901", true, 'valid'],
    ["2000", true, 'valid'],
    ["1900", true, 'valid'],
    ["2001", true, 'valid'],
    //YYYY&ZZXX
    //TODO should we issue a warning
    ["2014+0130", true, 'valid'],
    ["1901-0130", true, 'valid'],
    ["2000+1230", true, 'valid'],
    ["1900-1130", true, 'valid'],
    ["2001+0500", true, 'valid'],
    //YYYYMM
    ["201403", true, 'valid'],
    ["190101", true, 'valid'],
    ["200012", true, 'valid'],
    ["190002", true, 'valid'],
    ["200109", true, 'valid'],
    //YYYYMM&ZZXX
    ["201403+0130", true, 'valid'],
    ["190101-0215", true, 'valid'],
    ["200012+0330", true, 'valid'],
    ["190002-0445", true, 'valid'],
    ["200109+1200", true, 'valid'],
    //YYYYMMDD
    ["20140723", true, "valid"],
    ["19010101", true, "valid"],
    ["20001231", true, "valid"],
    ["20140723", true, "valid"],
    //YYYYMMDD&ZZXX
    ["20140723-1430", true, "valid"],
    ["19010101+1200", true, "valid"],
    ["20001231-0115", true, "valid"],
    ["20140723+0644", true, "valid"],
    //YYYYMMDDHH
    ["2014072300", true, "valid"],
    ["1901010101", true, "valid"],
    ["2000123102", true, "valid"],
    ["2014072312", true, "valid"],
    ["1583030315", true, "valid"],
    ["1800060620", true, "valid"],
    ["1999090923", true, "valid"],
    ["2010111123", true, "valid"],
    ["2014072300", true, "valid"],
    ["2014072300", true, "valid"],
    //YYYYMMDDHH&ZZXX
    ["2014072300+0101", true, "valid"],
    ["1901010101-0101", true, "valid"],
    ["2000123102+1111", true, "valid"],
    ["2014072312-1111", true, "valid"],
    ["1583030315+1212", true, "valid"],
    ["1800060620-1212", true, "valid"],
    ["1999090923+2332", true, "valid"],
    ["2010111123-1243", true, "valid"],
    ["2014072300+1154", true, "valid"],
    ["2014072300-1200", true, "valid"],
    //YYYYMMDDHHMM
    ["201407230000", true, "valid"],
    ["190101010101", true, "valid"],
    ["200012310220", true, "valid"],
    ["201407231212", true, "valid"],
    ["158303031515", true, "valid"],
    ["180006062020", true, "valid"],
    ["199909092359", true, "valid"],
    ["201011112359", true, "valid"],
    ["201407230000", true, "valid"],
    ["201407230000", true, "valid"],
    //YYYYMMDDHHMM&ZZXX
    ["201407230000+1159", true, "valid"],
    ["190101010101-1159", true, "valid"],
    ["200012310220+1049", true, "valid"],
    ["201407231212-1049", true, "valid"],
    ["158303031515+0950", true, "valid"],
    ["180006062020-0905", true, "valid"],
    ["199909092359+0840", true, "valid"],
    ["201011112359-0804", true, "valid"],
    ["201407230000+0707", true, "valid"],
    ["201407230000-0606", true, "valid"],
    //YYYYMMDDHHMMSS
    ["20140723000000", true, "valid"],
    ["19010101010101", true, "valid"],
    ["20001231022029", true, "valid"],
    ["20140723121212", true, "valid"],
    ["15830303151515", true, "valid"],
    ["18000606202020", true, "valid"],
    ["19990909235959", true, "valid"],
    ["20101111235950", true, "valid"],
    ["20140723000000", true, "valid"],
    ["20140723000000", true, "valid"],
    //YYYYMMDDHHMMSS&ZZXX
    ["20140723000000-0330", true, "valid"],
    ["19010101010101+0330", true, "valid"],
    ["20001231022029-0440", true, "valid"],
    ["20140723121212+0440", true, "valid"],
    ["15830303151515-0220", true, "valid"],
    ["18000606202020+0202", true, "valid"],
    ["19990909235959-0110", true, "valid"],
    ["20101111235950+0101", true, "valid"],
    ["20140723000000-0945", true, "valid"],
    ["20140723000000+1010", true, "valid"],
    //YYYYMMDDHHMMSS.FFFFFF
    ["20140723000000.0", true, "valid"],
    ["20140723000000.01", true, "valid"],
    ["19010101010101.012", true, "valid"],
    ["20001231022029.0123", true, "valid"],
    ["20140723121212.01234", true, "valid"],
    ["15830303151515.012345", true, "valid"],
    ["18000606202020.543210", true, "valid"],
    ["19990909235959.432100", true, "valid"],
    ["20101111235950.321000", true, "valid"],
    ["20140723000000.21000  ", true, "valid"],
    ["20140723000000.1000   ", true, "valid"],
    //YYYYMMDDHHMMSS.FFFFFF&ZZXX
    ["20140723000000.0+1200", true, "valid"],
    ["20140723000000.01-0531", true, "valid"],
    ["19010101010101.012+0000", true, "valid"],
    ["20001231022029.0123-0045", true, "valid"],
    ["20140723121212.01234+0155", true, "valid"],
    ["15830303151515.012345-0515", true, "valid"],
    ["18000606202020.543210+1430", true, "valid"],
    ["19990909235959.432100-1100", true, "valid"],
    ["20101111235950.321000+0730", true, "valid"],
    ["20140723000000.21000-0323  ", true, "valid"],
    ["20140723000000.1000+0759   ", true, "valid"],
    //YYYYMMDDHHMMSS
    ["19010101010101", true, "valid"],
    ["20001231022029 ", true, "valid"],
    ["20140723121212  ", true, "valid"],
    ["15830303151515   ", true, "valid"],
    ["18000606202020  ", true, "valid"],
    ["19990909235959  ", true, "valid"],
    ["20101111235950            ", true, "valid"]];
// Strings with valid YearMonthDate dates
List<List> invalidDateTimeStrings
    = [//YYYY
       ["0000", false, 'invalid'],
       ["19x1", false, 'invalid'],
       ["200x", false, 'invalid'],
       ["1 00", false, 'invalid'],
       [" 001", false, 'invalid'],
       //YYYY&ZZXX
       //TODO should we issue a warning?
       ["0000+0130", false, 'invalid'],
       ["19x1-0130", false, 'invalid'],
       ["2x00+1230", false, 'invalid'],
       [" 900-1130", false, 'invalid'],
       ["2 01+0500", false, 'invalid'],
       //YYYYMM
       ["2014 3", false, 'invalid'],
       ["1901x1", false, 'invalid'],
       ["20001 ", false, 'invalid'],
       ["19 002", false, 'invalid'],
       ["200x09", false, 'invalid'],
       //YYYYMM&ZZXX
       ["2014 3+0130", false, 'invalid'],
       ["1901x1-0215", false, 'invalid'],
       ["200011+0 30", false, 'invalid'],
       ["19 002-0445", false, 'invalid'],
       ["200109+12x0", false, 'invalid'],
       //YYYYMMDD
       ["201407x3", false, "invalid"],
       ["19010x01", false, "invalid"],
       ["2000x231", false, "invalid"],
       ["201x0723", false, "invalid"],
       //YYYYMMDD&ZZXX
       ["20140723-143 ", false, "invalid"],
       ["19010101+1 00", false, "invalid"],
       ["20001231-x115", false, "invalid"],
       ["20140723+0x44", false, "invalid"],
       //YYYYMMDDHH
       ["20140723 0", false, "invalid"],
       ["190101010 ", false, "invalid"],
       ["20001231x2", false, "invalid"],
       ["201407231x", false, "invalid"],
       ["1583030324", false, "invalid"],
       ["1800060625", false, "invalid"],
       ["1999093223", false, "invalid"],
       ["2010114023", false, "invalid"],
       ["2014142300", false, "invalid"],
       ["2014162300", false, "invalid"],
       //YYYYMMDDHH&ZZXX
       ["2014142300+01x1", false, "invalid"],
       ["1901130101-010x", false, "invalid"],
       ["2000123202+1 11", false, "invalid"],
       ["2014072325-x111", false, "invalid"],
       ["1583000315+1212", false, "invalid"],
       ["1800000620-12-2", false, "invalid"],
       ["1999094023+2332", false, "invalid"],
       ["2010111125-3493", false, "invalid"],
       ["2014072300+4554", false, "invalid"],
       ["2014072300-5665", false, "invalid"],
       //YYYYMMDDHHMM
       ["201402300000", false, "invalid"],
       ["190101012501", false, "invalid"],
       ["200012310260", false, "invalid"],
       ["111107231212", false, "invalid"],
       ["158103031515", false, "invalid"],
       ["180013062020", false, "invalid"],
       ["199960092359", false, "invalid"],
       ["201011112360", false, "invalid"],
       ["20140723-100", false, "invalid"],
       ["201407340000", false, "invalid"],
       /*
       //YYYYMMDDHHMM&ZZXX
       ["201407230000+1159", false, "invalid"],
       ["190101010101-1159", false, "invalid"],
       ["200012310220+1049", false, "invalid"],
       ["201407231212-1049", false, "invalid"],
       ["158303031515+0950", false, "invalid"],
       ["180006062020-0905", false, "invalid"],
       ["199909092359+0840", false, "invalid"],
       ["201011112359-0804", false, "invalid"],
       ["201407230000+0707", false, "invalid"],
       ["201407230000-0606", false, "invalid"],
       //YYYYMMDDHHMMSS
       ["20140723000000", false, "invalid"],
       ["19010101010101", false, "invalid"],
       ["20001231022029", false, "invalid"],
       ["20140723121212", false, "invalid"],
       ["15830303151515", false, "invalid"],
       ["18000606202020", false, "invalid"],
       ["19990909235959", false, "invalid"],
       ["20101111235950", false, "invalid"],
       ["20140723000000", false, "invalid"],
       ["20140723000000", false, "invalid"],
       //YYYYMMDDHHMMSS&ZZXX
       ["20140723000000-0330", false, "invalid"],
       ["19010101010101+0330", false, "invalid"],
       ["20001231022029-0440", false, "invalid"],
       ["20140723121212+0440", false, "invalid"],
       ["15830303151515-0220", false, "invalid"],
       ["18000606202020+0202", false, "invalid"],
       ["19990909235959-0110", false, "invalid"],
       ["20101111235950+0101", false, "invalid"],
       ["20140723000000-0945", false, "invalid"],
       ["20140723000000+1010", false, "invalid"],
       //YYYYMMDDHHMMSS.FFFFFF
       ["20140723000000.0", false, "invalid"],
       ["20140723000000.01", false, "invalid"],
       ["19010101010101.012", false, "invalid"],
       ["20001231022029.0123", false, "invalid"],
       ["20140723121212.01234", false, "invalid"],
       ["15830303151515.012345", false, "invalid"],
       ["18000606202020.543210", false, "invalid"],
       ["19990909235959.432100", false, "invalid"],
       ["20101111235950.321000", false, "invalid"],
       ["20140723000000.21000  ", false, "invalid"],
       ["20140723000000.1000   ", false, "invalid"],
       //YYYYMMDDHHMMSS.FFFFFF&ZZXX
       ["20140723000000.0+1200", false, "invalid"],
       ["20140723000000.01-0531", false, "invalid"],
       ["19010101010101.012+0000", false, "invalid"],
       ["20001231022029.0123-0045", false, "invalid"],
       ["20140723121212.01234+0155", false, "invalid"],
       ["15830303151515.012345-0515", false, "invalid"],
       ["18000606202020.543210+1430", false, "invalid"],
       ["19990909235959.432100-1100", false, "invalid"],
       ["20101111235950.321000+0730", false, "invalid"],
       ["20140723000000.21000-0323  ", false, "invalid"],
       ["20140723000000.1000+0759   ", false, "invalid"],
       //YYYYMMDDHHMMSS
       ["19010101010161",    false, "invalid"],
       ["20001231022029 v",  false, "invalid"],
       ["20140723121212  x", false, "invalid"],
       ["15830303151515  2", false, "invalid"],
       ["18000606202020 1",  false, "invalid"],
       ["19990909235959  1", false, "invalid"],
       ["20101111235950            0", false, "invalid"]
       */
       ];

// String with valid YearMonth dates
List<List> validYearMonthOnlyDateStrings
    = [["201403", true, 'valid'],
       ["190101", true, 'valid'],
       ["200012", true, 'valid'],
       ["190002", true, 'valid'],
       ["200109", true, 'valid']];

// Strings with valid Year only
List<List> validYearOnlyDateStrings
    = [["2014", true, 'valid'],
       ["1901", true, 'valid'],
       ["2000", true, 'valid'],
       ["1900", true, 'valid'],
       ["2001", true, 'valid']];

List invalidDatesWithSpaces
    = [["1950011 ", false, "spaces in string"],
       ["195001 1", false, "spaces in string"],
       ["1950  01", false, "spaces in string"],
       ["1950 101", false, "spaces in string"],
       ["195 0101", false, "spaces in string"],
       ["19 00101", false, "spaces in string"],
       ["1 500101", false, "spaces in string"],
       [" 9500101", false, "spaces in string"]];

// Strings with bad date formats
List<List> invalidDateStrings
   = [["19000231",  false, "February doesn't have 31 days"],
      ["20010931",  false, "September doesn't have 31 days"],
      ["201407310", false, '9 characters'],
      ["2014073",   false, '7 characters'],
      ["20140",     false, '5 characters'],
      ["201",       false, '3 characters'],
      ["190101x0",  false, '"x" in day'],
      ["20001x31",  false, '"x" in month'],
      ["19x00031",  false, '"x" in year'],
      ["9999  31",  false, 'internal spaces']];


typedef dynamic Parser(String);
typedef dynamic LeapYearTester(String);

void main() {

  void dateTimeParserTest(Parser parser, String name) {

    group('$name: Date Parser', () {

      test('Valid YearMonthDay dates', () {
        for(List list in validDateTimesWithoutTimeZones) {
          String date = list[0];
          String reason = list[2];
          //print(date);
         expect(parser(date), equals(date), reason: reason);
        }
      });
/*
      test('Valid YearMonth only dates', () {
        for(List list in validYearMonthOnlyDateStrings) {
          String date = list[0];
          String reason = list[2];
         expect(parser(date), equals(date), reason: reason);
        }
      });

      test('Valid Year only dates', () {
        for(List list in validYearOnlyDateStrings) {
          String date = list[0];
          String reason = list[2];
         expect(parser(date), equals(date), reason: reason);
        }
      });
*/
      test('Invalid dates', () {
        for(List list in invalidDateTimeStrings) {
          String date = list[0];
          String reason = list[2];
          //print(date);
         expect((){parser(date);}, throws, reason: reason);
        }
      });
/*
      test('Invalid dates with Spaces', () {
        for(List list in invalidDatesWithSpaces) {
          String date = list[0];
          String reason = list[2];
          //print(date);
         expect((){parser(date);}, throws, reason: reason);
        }
      });
*/
    });

  }

  String parseDateTimeString(String s) {
    DateTimeReader input = new DateTimeReader.fromString(s);
    DateTime date = input.readDateTime();
    return date.toString();
  }

  String parseDateTimeBytes(String s) {
    Uint8List bytes = new Uint8List.fromList(s.codeUnits);
    DateTimeReader input = new DateTimeReader(bytes);
    DateTime date = input.readDateTime();
    return date.toString();
  }

  // delete when debugged

  for(List list in validDateTimesWithoutTimeZones) {
    String date = list[0];
    String reason = list[2];
    print('string=$date length=${date.length}');
    print(parseDateTimeString(date));
  }

}