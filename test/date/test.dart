// interactive tests

//import 'dart:math';
import 'dart:core' hide DateTime;
import 'package:core/src/date/date.dart';
import 'package:core/src/date/date_time.dart';
import 'package:core/src/date/date_time_reader.dart';
import 'package:core/src/date/time.dart';

List<String> validBasicDates = ["15820101", "158201", "1582",
                                "19500718", "195007", "1950",
                                "20001010 ", "200010  ", "2000   ",
                                "20040229", "200402", "2004", //leap year
                                "20000229", "200002", "2004", //common year
                                "99991231", "999912", "9999"];

List<String> invalidBasicDates = ["15810101",  "158101",  "1581",     // before firstYear
                                  "00010718",  "000107",  "0001",     // after lastYear
                                  "2000101x0", "20001x0", "200x0",    //illegal char
                                  "20040231",  "200400",  "10000",    //leap year
                                  "2000000",   "200013",  "1492",     //common year
                                  "2000101 0", "20001 0", "200 0",    //embedded space
                                  " 99991231",  " 999912",  " 9999"]; //leading space

List<String> validXDates = ["1582-01-01", "1582-01", "1582",
                            "1950-07-18", "1950-07", "1950",
                            "2000-10-10 ", "2000-10  ", "2000   ",
                            "2004-02-29", "2004-02", "2004", //leap year
                            "2000-02-29", "2000-02", "2004", //common year
                            "9999-12-31", "9999-12", "9999"];

List<String> invalidXDates = ["1581-01-01",  "1581-01",   "1581",     // before firstYear
                              "0001-07-18",  "0001-07",   "0001",     // after lastYear
                              "2000-10-1x0", "2000-1x0",  "200x0",    //illegal char
                              "2004-02-31",  "2004-00",   "10000",    //leap year
                              "2000-00-0",   "2000-13",   "1492",     //common year
                              "2000-10-1 0", "2000-1 -0", "200 0",    //embedded space
                              " 9999-12-31", " 9999-12",  " 9999"];   //leading space

void testValidBasicDates(List list) {
  print('***** testValidBasicDates');
  DateTimeReader rd;
  for(String source in list) {

    print(' in=\"$source\"');
    rd = new DateTimeReader(source.codeUnits);
    Date date = rd.readDate();
    String result = date.dicomString;
    bool same = source == result;
    print('out=\"$result\"');
    if (!same) date.debug();
    print('____');
  }
}

void testInvalidBasicDates(List list) {
  print('***** testInvalidBasicDates');
  DateTimeReader rd;
  Date date;
  for(String source in list) {
    print('src=\"$source\"');
    rd = new DateTimeReader(source.codeUnits);
    try {
      date = rd.readDate();
    } on Exception catch(e) {             // Anything else that is an exception
      print('Error: s=\"$source\",\n $e');
      print('____');
      continue;
    }
    String result = date.dicomString;
    bool same = source == result;
    print('valid input when testing ');
    print('Error: passed test that should file:out=\"$result\"');
    if (!same) date.debug();
    print('____');
  }
}

void testValidXDates(List list) {
  print('***** testValidXDates');
  DateTimeReader rd;
  for(String source in list) {
    print('src=\"$source\"');
    rd = new DateTimeReader(source.codeUnits);
    Date date = rd.readDate();
    String result = date.httpString;
    bool same = source == result;
    print('out=\"$result\"');
    if (!same) date.debug();
    print('____');
  }
}

void testInvalidXDates(List list) {
  print('***** testInvalidXDates');
  DateTimeReader rd;
  Date date;
  for(String source in list) {
    print('src=\"$source\"');
    rd = new DateTimeReader(source.codeUnits);
    try {
      date = rd.readDate();
    } on Exception catch(e) {             // Anything else that is an exception
      print('Error: s=\"$source\",\n $e');
      print('____');
      continue;
    }
    String result = date.httpString;
    bool same = source == result;
    print('valid input when testing ');
    print('Error: passed test that should file:out=\"$result\"');
    if (!same) date.debug();
    print('____');
  }
}

List<String> validBasicTimes = ["000000",  "0000",   "00",
                                "235959",  "2359",   "23",
                                "120101 ", "1201  ", "12   ",
                                "121010",  "1210",   "09", //leap year
                                "123456",  "1234",   "12", //common year
                                "012345",  "0123",   "01"];

List<String> invalidBasicTimes = ["15810101",  "158101",  "1581",     // before firstYear
                                  "00010718",  "00010",   "000",     // after lastYear
                                  "2000101x0", "20001x0", "200x0",    //illegal char
                                  "20040231",  "20040",   "10000",    //leap year
                                  "2000000",   "20001",   "1492",     //common year
                                  "2000101 0", "20001 0", "200 0",    //embedded space
                                  " 99991231",  " 999912",  " 9999"]; //leading space

List<String> validXTimes
    = [ // Simple times
       "00:00:00",  "00:00",   "00",
       "23:59:59",  "23:59",   "23",
       "05:40:18",  "05:40",   "05",
       "01:01:01",  "01:01",   "01",
       "10:10:10",  "10:10",   "10",
       "22:33:44",  "22:33",   "22",
        // Times w/ trailing spaces
       "10:10:10 ",  "10:10  ", "10   ",
       "23:59:59  ", "23:59  ", "23   ",
        // Times w/ fractional second
       "20:10:10.123456 ", "10:10:10.12345",  "10:10:00.1234",
       "10:10:10.123",     "22:33:44.12",     "10:10:10.1 ",
       "20:10:10,012 ",    "20:10:20,9876  ", "20:10:10,456   ",
        // Times w/ timezone
       "01:01:01+0000",   "01:01+0000",        "01+0000",
       "10:10:10Z",       "10:10Z",            "10Z",
       "10:10:10+05",     "10:10-06:30",       "10Z",
       "10:20:30.1+05",   "10:20:30.123+12",   "10:20:30.123456-14"
       "01:01:01.1-0830", "01:01:01.123+0830", "01:01:01.123456-1030"
       ];

List<String> invalidXTimes
    = ["15:01:-1",    "15:-1",     "-1",     // before firstYear
       "01:07:61",    "00:60",     "24",     // after lastYear
       "00:10:1x0",   "20:1x0",    "2x0",    //illegal char
       "04:02:301",    "20:040:00",    "010:00", // wrong number of chars
       "2000:00:0",   "2000:13",   "1492",     //common year
       "2000:10:1 0", "2000:1 :0", "200 0",    //embedded space
       " 9999:12:31", " 9999:12",  " 9999"]; //leading space

void testValidBasicTimes(List list) {
  print('***** testValidBasicTimes');
  DateTimeReader rd;
  for(String source in list) {
    print('src=\"$source\"');
    rd = new DateTimeReader(source.codeUnits);
    Time time = rd.readTime();
    String result = time.dicomString;
    bool same = source == result;
    print('out=\"$result\"');
    if (!same) time.debug();
    print('____');
  }
}

void testInvalidBasicTimes(List list) {
  print('***** testInvalidBasicTimes');
  DateTimeReader rd;
  Time time;
  for(String source in list) {
    print('src=\"$source\"');
    rd = new DateTimeReader(source.codeUnits);
    try {
      time = rd.readTime();
    } on Exception catch(e) {             // Anything else that is an exception
      print('Error: s=\"$source\",\n $e');
      print('____');
      continue;
    }
    String result = time.dicomString;
    bool same = source == result;
    print('valid input when testing ');
    print('Error: passed test that should fail:out=\"$result\"');
    if (!same) time.debug();
    print('____');
  }
}

void testValidXTimes(List list) {
  print('***** testValidXTimes');
  DateTimeReader rd;
  for(String source in list) {
    print('src=\"$source\"');
    rd = new DateTimeReader(source.codeUnits);
    Time time = rd.readTime();
    String result = time.httpString;
    bool same = source == result;
    print('out=\"$result\"');
    if (!same) time.debug();
    print('____');
  }
}

void testInvalidXTimes(List list) {
  print('***** testInvalidXTimes');
  DateTimeReader rd;
  Time time;
  for(String source in list) {
    print('src=\"$source\"');
    rd = new DateTimeReader(source.codeUnits);
    try {
      time = rd.readTime();
    } on Exception catch(e) {             // Anything else that is an exception
      print('Error: s=\"$source\",\n $e');
      print('____');
      continue;
    }
    String result = time.httpString;
    bool same = source == result;
    print('valid input when testing ');
    print('Error: passed test that should file:out=\"$result\"');
    if (!same) time.debug();
    print('____');
  }
}

void main () {

  //testValidBasicDates(validBasicDates);
  //testInvalidBasicDates(invalidBasicDates);
  //testValidXDates(validXDates);
  //testInvalidXDates(invalidXDates);
  //testValidBasicTimes(validBasicTimes);
  //testInvalidBasicTimes(invalidBasicTimes);
//  testValidXTimes(validXTimes);
  //testInvalidXTimes(invalidXTimes);

  DateTimeReader rd;
  String s;
  Date date;
  Time time;
  DateTime dateTime;
/*
  s = '19500718';
  print(s);
  rd = new DateTimeReader(s.codeUnits);
  date = rd.date;
  date.debug();
*/
/*
  s = " 1950-12-03";
  print(s);
  rd = new DateTimeReader.fromString(s);
  date = rd.readXDate();
  date.debug();
*/

  s = "233030+0530";
  print(s);
  rd = new DateTimeReader(s.codeUnits);
  time = rd.time;
  time.debug();

  print(s);
  rd = new DateTimeReader.fromString(s);
  time = rd.time;
  time.debug();

/*
  s = "10:10:10.1 ";
  print(s);
  rd = new DateTimeReader.fromString(s);
  time = rd.readXTime();
  time.debug();
*/
/*
  s = "19500718123005";
  print(s);
  rd = new DateTimeReader(s.codeUnits);
  DateTime dateTime = rd.readDateTime();
  dateTime.debug();

  s = "1950-12-03T12:06:31";
  print(s);
  rd = new DateTimeReader.fromString(s);
  dateTime = rd.readXDateTime();
  dateTime.debug();
*/

}