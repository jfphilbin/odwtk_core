// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library leap_second_test;

import 'package:core/src/date/leap_seconds.dart';

void main() {

  LeapSecond ls1 = LeapSecond.lookup(1972, 6);
  LeapSecond ls2 = LeapSecond.lookup(1972, 12);
  //LeapSecond ls3 = new LeapSecond(1973, 8);
  print(LeapSecond.lookup(1972, 6));
  print('ls1 = $ls1');
  print('Is 19720630235960 a valid leap second? ${LeapSecond.isValid(1972, 06, 30, 23, 59, 60)}');
  print(ls1.dateTime);

  DateTime now = new DateTime.now();
  print(now);
  print(now.timeZoneName);
  print(now.timeZoneOffset.runtimeType);
}