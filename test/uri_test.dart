// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library uri_test;

import 'package:uri/uri.dart';

main() {

  UriTemplate base_template = new UriTemplate('http:{service}/studies');
// Retrieve Templates
  UriTemplate retrieve_study_template    = new UriTemplate('/{studyUID}');
  UriTemplate retrieve_series_template   = new UriTemplate('/{studyUID}/series/{series}');
  UriTemplate retrieve_instance_template = new UriTemplate('/{studyUID}/series/{series}/instance/{instance)');
// Store Templates
  UriTemplate store_study_template = new UriTemplate('/{studyUID}');
// Query Templates
  UriTemplate query_study_template    = new UriTemplate('/{studyUID}?{query}');
  UriTemplate query_series_template   = new UriTemplate('/{studyUID}/series/{series}?{query}');
  UriTemplate query_instance_template = new UriTemplate('/{studyUID}/series/{series}/{instance}?{query}');


  Map<String, Object> test = {'service':     'mint.org/dicomweb',
                              'studyUID':    '2.52.1',
                              'seriesUID':   '2.52.2',
                              'instanceUID': '2.52.3' };

  print('foo');
  //String base = base_template.expand(test);

  //print(base);
}
