// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

/// A program to test lookup table performance

import 'dart:convert';
import 'dart:io';

import 'package:core/core_error.dart';
import 'package:dictionary/dictionary.dart';


void main() {
  Map<int, Map> table1 = new Map<int, Map>();


  int _group(int tag) => (tag & 0xFFFF0000) >> 16;
  int _element(int tag) => tag & 0xFFFF;

  for(int i = 0; i < tagList.length; i++) {
    Tag tag = tagList[i];
    int value = tag.code;

    int index1 = _group(value);

    Map table2 = table1[index1];
    if (table2 == null) {
      table2 = new Map<int, Tag>();
      table1[index1] = table2;
    }
    int index2 = _element(value);
    if (table2[index2] == null)
      table2[index2] = tag;
    else
      error('Tag Definition already exists');
  }
  print(table1);

  File file = new File('tag_table.json');
  String s = JSON.encode(table1);
  print(s);
  file.writeAsStringSync(s);
}

class TagTable {
  Map<int, Map> _table1 = new Map<int, Map>();

  TagTable();

  int _group(int tag) => (tag & 0xFFFF0000) >> 16;
  int _element(int tag) => tag & 0xFFFF;

  void add(int tag, Tag def) {
    int index1 = _group(tag);

    Map table2 = _table1[index1];
    if (table2 == null) {
      table2 = new Map<int, Tag>();
      _table1[index1] = table2;
    }
    int index2 = _element(tag);
    if (table2[index2] == null)
      table2[index2] = def;
    else
      error('Tag Definition already exists');
  }

  Tag lookup(int tag) {
    Map<int, Tag> table2 = _table1[_group(tag)];
    if (table2 == null) error('Empty element table entry');
    return table2[_element(tag)];
  }

}


TagTable initializeTagTable() {
  TagTable table = new TagTable();

  table.add(0x0020000D,  Tag.StudyInstanceUID);
  table.add(0x0020000E,  Tag.SeriesInstanceUID);
  table.add(0x00080018,  Tag.SOPInstanceUID);

  return table;
}

TagTable tagTable = initializeTagTable();