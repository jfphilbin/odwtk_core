## DWDK<sup>*</sup> Core Package##

This package contains the classes that form the core of DWTK. They form the base set of classes used to create DICOM or Multi-Study DICOM (MSD). It contains classes such as **VM**, **VR**, **UID**, **Tag**, etc.  Each class in *core* is a separate library and includes any imports that it might need.  This allows other packages to use only the classes they need.


**\*DICOMweb Toolkit**