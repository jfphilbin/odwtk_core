// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library dataset;

import 'dart:collection';
import 'package:core/attribute.dart';
import 'package:core/dataset_type.dart';
//import 'package:core/sequence.dart';
import 'package:dictionary/vr.dart';

/*
 * A DICOM Dataset
 *
 * This is the base type for Dataset.  A Dataset is a [Map] of [Tag], [Value] pairs,
 * in ascending order by [Tag].  All Map operations work on Dataset.
 *
 * See docLink for more information.
 */
class Dataset extends Maps {
  // The Type of the Dataset, see DSType above.
  final DSType              type;
  final Dataset             parent;
  //TODO decide if a back pointer to sequence is useful!
  //final Sequence           sequence;
  final Map<int, Attribute> attributes = new Map();
  //final Map<int, dynamic> attributes = new SplayTreeMap();

  final String docLink = "doc/dicom/dart/dataset.html";

  //Dataset._internal(this.type);
  //Dataset(this.type, this.parent, this.sequence);
  Dataset(this.type, this.parent);

  factory Dataset.top() =>  new Dataset(DSType.TOP, null);

  // The basic map primitives to extend Maps
  Iterable<int> get keys => attributes.keys;
  dynamic operator [](int tag) => attributes[tag];
  void    operator []=(int tag, value) => attributes[tag] = value;
  dynamic remove(Object key) => attributes.remove(key);

  // Synonym for []=
  //TODO keep track of the last [Tag] added and make sure the order is always increasing.
  void add(int tag, VR vr, dynamic value) => attributes[tag] = value;



}

