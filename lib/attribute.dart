// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library attribute;

import 'package:dictionary/dictionary.dart';

/**
 * A single DICOM [Attribute].
 */
class Attribute {
  int tag;
  VR  vr;
  var value;

  Attribute(this.tag, this.vr, this.value);

  bool operator == (Attribute a) {
    if ((a.tag != tag) || (a.vr != vr) || (a.value != value)) return false;
    else return true;
  }

  String toString() => "Attribute[tag=${tagToHex(tag)}, vr=${vr.name}, value=$value]";
}
