// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library dicom_file;

/**
 * [this] is the representation of a file that has or will contain a DICOM object.  The
 * object could be a DICOM PS 3.10 (Part 10) file.
 */

import 'dart:io';
//import 'dart:typed_data';

//import 'package:anonymization/anonymize.dart';
import 'package:dictionary/dictionary.dart';
import 'package:core/core.dart';
//import 'package:io/io.dart';

import 'package:path/path.dart' as Path;

class DicomFile {
	String            _filename = "";
	File              _file;
	//TODO decide if we should use a [Dataset] or an [FileMetaInfo] for this.
	//FileMetaInfo    _fmi;
	Dataset           _fmi;
	Dataset           _dataset;
  bool              _isLoaded; // Indicates if <see cref="Load"/> has been called on this file.
  bool              _wasSaved;

  // The length of the FMI in the file.  This value is only valid if [isLoaded]
  // or [wasSaved] is [true].
  int _fmiLengthInFile;

  // Getters and Setters
  /// The filename of the file.
  String get filename => _filename;
  void   set filename(String name) { _filename = name; }

  File    get file    => (_file == null) ? new File(_filename) : _file;
	Dataset get fmi       => _fmi;
	Dataset get dataset  => _dataset;
	bool    get isLoaded => _isLoaded;
	int     get fmiLength => _fmiLengthInFile;

	/**
	 * Create a DicomFile instance.
	 */
   DicomFile([this._filename, this._fmi, this._dataset]) {
     if (_filename == null) _filename = "";

     if (_fmi == null) {
       _fmi= new Dataset(DSType.FILE_META_INFO, null);
       //TODO fix
       ImplementationVersionName = Version.name;
       ImplementationClassUID = Version.classUID;
     } else {
       if (_fmi[$ImplementationVersionName] == null)
				ImplementationVersionName = Version.name;
			if (!_fmi[$ImplementationClassUID] == null) {
			  //TODO fix
				//ImplementationClassUid = DicomImplementation.ClassUID.UID;
			}
     }

			// If the FMI doesn't already specify the transfer syntax, give it
			// the default transfer syntax of ELE
			String  tsUid = _fmi[$TransferSyntaxUID].ToString();
			if ((tsUid == null) || (tsUid == ""))
			  //TODO should this be Explicit or Implicit
				_fmi[$TransferSyntaxUID] = UID.ImplicitVRLittleEndianDefaultTransferSyntaxforDICOM;

			//TODO what should default version be?
			if (!_fmi[$FileMetaInformationVersion])
				_fmi[$FileMetaInformationVersion] = [0x00, 0x01];

			if (_dataset == null) _dataset = new Dataset.top();
   }

		/// Creates a new DicomFile instance from an existing <see cref="DicomMessage"/> instance.
    ///
		/// This routine assigns the existing <see cref="DicomMessage.DataSet"/> into the new
		/// DicomFile instance.
    ///
		/// A new <see cref="DicomAttributeCollection"/> is created for the MetaInfo.  The
		/// Media Storage SOP Instance UID, Media Storage SOP Class UID, Implementation Version Name,
		/// and Implementation Class UID tags are automatically set in the FMI information.
		//TODO fix
   /*
	  DicomFile.message(DicomMessage msg, String filename) {
			Dataset _fmi = new Dataset(DSType.FILE_META_INFO, null);
			Dataset _dataSet = msg.DataSet;

			MediaStorageSopInstanceUid = msg.AffectedSopInstanceUid;
			MediaStorageSopClassUid = msg.AffectedSopClassUid;
			ImplementationVersionName = DicomImplementation.Version;
			ImplementationClassUid = DicomImplementation.ClassUID.UID;
			if (msg.TransferSyntax.Encapsulated) {
				_fmi[$TransferSyntaxUID] = msg.TransferSyntax;
			} else {
				_fmi[$TransferSyntaxUID] = UID.ExplicitVRLittleEndian;
			}
			_fmi[$FileMetaInformationVersion] = [0x00, 0x01];

			_filename = filename;
		}
   */


		/// The SOP Class of the file.
    ///
		/// This property returns a <see cref="SopClass"/> object for the sop class
		/// encoded in the tag Media Storage SOP Class UID (0002,0002).
		//TODO define SOPClass
		UID get sopClass => _fmi[$MediaStorageSOPClassUID];


		/// The transfer syntax the file is encoded in.
    ///
		/// This property returns a TransferSyntax object for the transfer syntax encoded
		/// in the tag Transfer Syntax UID (0002,0010).
    //TODO do we want TransferSyntax to be a type?
		//override TransferSyntax TransferSyntax
		UID  get transferSyntax => _fmi[$TransferSyntaxUID];
	  void set transferSyntax(UID ts) { _fmi[$TransferSyntaxUID] = ts; }

	  //TODO fix
	  //TODO create DicomSteamOpener or DicomFileOpener
		File get streamOpener {
      if ((_filename == null) || (_filename == "")) {
        _file =  _file = null;
      } else {
        _file =  new File(_filename);
      }
      return _file;
		}

		/*
		void set streamOpener(value) {
				_file = value;
				_filename = "";
		}
		 */

		/// Uniquiely identifies the SOP Class associated with the Data Set.  SOP Class
		/// UIDs allowed for media storage are specified in PS3.4 of the DICOM Standard
		/// - Media Storage Application Profiles.
		String get MediaStorageSopClassUID => _fmi[$MediaStorageSOPClassUID];
		void   set MediaStorageSopClassUID(String uid) { _fmi[$MediaStorageSOPClassUID] = uid; }

		/// Uniquiely identifies the SOP Instance associated with the Data Set placed
		/// in the file and following the File Meta Information.
		String get MediaStorageSopInstanceUID =>_fmi[$MediaStorageSOPInstanceUID];
	  void   set MediaStorageSopInstanceUID(String uid) { _fmi[$MediaStorageSOPInstanceUID] = uid; }

		/// Uniquely identifies the implementation which wrote this file and its content.
		/// It provides an unambiguous identification of the type of implementation which
		/// last wrote the file in the event of interchagne problems.  It follows the
		/// same policies as defined by PS 3.7 of the DICOM Standard (association negotiation).
		String get ImplementationClassUID => _fmi[$ImplementationClassUID];
	  void   set ImplementationClassUID(String uid) { _fmi[$ImplementationClassUID] = uid; }


		/// Identifies a version for an Implementation Class UID (002,0012) using up to
		/// 16 characters of the repertoire.  It follows the same policies as defined in
		/// PS 3.7 of the DICOM Standard (association negotiation).
		String get ImplementationVersionName =>_fmi[$ImplementationVersionName];
		void   set ImplementationVersionName(String name) { _fmi[$ImplementationVersionName] = name; }

		/// Uniquely identifies the Transfer Syntax used to encode the following Data Set.
		/// This Transfer Syntax does not apply to the File Meta Information.
		String get TransferSyntaxUID => _fmi[$TransferSyntaxUID];
		void   set TransferSyntaxUID(String uid) { _fmi[$TransferSyntaxUID] = uid; }

		/// The DICOM Application Entity (AE) Title of the AE which wrote this file's
		/// content (or last updated it).  If used, it allows the tracing of the source
		/// of errors in the event of media interchange problems.  The policies associated
		/// with AE Titles are the same as those defined in PS 3.8 of the DICOM Standard.
		String get SourceApplicationEntityTitle =>_fmi[$SourceApplicationEntityTitle];
		void   set SourceApplicationEntityTitle(String aeTitle) { _fmi[$SourceApplicationEntityTitle] = aeTitle; }


		/// Identifies a version for an Implementation Class UID (002,0012) using up to
		/// 16 characters of the repertoire.  It follows the same policies as defined in
		/// PS 3.7 of the DICOM Standard (association negotiation).
		String get PrivateInformationCreatorUID => _fmi[$PrivateInformationCreatorUID];
		void   set PrivateInformationCreatorUID(String uid) { _fmi[$PrivateInformationCreatorUID] = uid; }

/*
		/// Load a DICOM file with the default <see cref="DicomReadOptions"/> set.
    ///
		/// Note:  If the file does not contain DICM encoded in it, the routine will assume
		/// the file is not a Part 10 format file, and is instead encoded as just a DataSet
		/// with the transfer syntax set to Implicit VR Little Endian.
		/// </remarks>
		void Load([String filename]) {
			Platform. CheckForEmptyString(filename, "filename");
			_filename = filename;
			Load(DicomReadOptions.Default);
		}

		/// Load a DICOM file (as set by the <see cref="Filename"/> property) with the
		/// default <see cref="DicomReadOptions"/> set.
    ///
		/// Note:  If the file does not contain DICM encoded in it, the routine will assume
		/// the file is not a Part 10 format file, and is instead encoded as just a DataSet
		/// with the transfer syntax set to Implicit VR Little Endian.
		/// </remarks>
		void Load()
		{
			Load(DicomReadOptions.Default);
		}

		/// <summary>
		/// Load a DICOM file.
		/// </summary>
		/// <remarks>
		/// Note:  If the file does not contain DICM encoded in it, the routine will assume
		/// the file is not a Part 10 format file, and is instead encoded as just a DataSet
		/// with the transfer syntax set to Implicit VR Little Endian.
		/// </remarks>
		/// <param name="options">The options to use when reading the file.</param>
		/// <param name="filename">The path of the file to load.</param>
		void Load(DicomReadOptions options, String filename)
		{
			Platform.CheckForEmptyString(filename, "filename");
			Filename = filename;
			Load(null, options);
		}

		/// <summary>
		/// Load a DICOM file (as set by the <see cref="Filename"/> property).
		/// </summary>
		/// <remarks>
		/// Note:  If the file does not contain DICM encoded in it, the routine will assume
		/// the file is not a Part 10 format file, and is instead encoded as just a DataSet
		/// with the transfer syntax set to Implicit VR Little Endian.
		/// </remarks>
		/// <param name="options">The options to use when reading the file.</param>
		void Load(DicomReadOptions options)
		{
			Load(null, options);
		}

		/// <summary>
		/// Load a DICOM file.
		/// </summary>
		/// <remarks>
		/// Note:  If the file does not contain DICM encoded in it, the routine will assume
		/// the file is not a Part 10 format file, and is instead encoded as just a DataSet
		/// with the transfer syntax set to Implicit VR Little Endian.
		/// </remarks>
		/// <param name="stopTag">A tag to stop at when reading the file.  See the constants in <see cref="DicomTags"/>.</param>
		/// <param name="options">The options to use when reading the file.</param>
		/// <param name="filename">The path of the file to load.</param>
		void Load(uint stopTag, DicomReadOptions options, String filename)
		{
			Platform.CheckForEmptyString(filename, "filename");
			Filename = filename;
			Load(stopTag, options);
		}

		/// <summary>
		/// Load a DICOM file (as set by the <see cref="Filename"/> property).
		/// </summary>
		/// <remarks>
		/// Note:  If the file does not contain DICM encoded in it, the routine will assume
		/// the file is not a Part 10 format file, and is instead encoded as just a DataSet
		/// with the transfer syntax set to Implicit VR Little Endian.
		/// </remarks>
		/// <param name="stopTag">A tag to stop at when reading the file.  See the constants in <see cref="DicomTags"/>.</param>
		/// <param name="options">The options to use when reading the file.</param>
		void Load(uint stopTag, DicomReadOptions options)
		{
			DicomTag stopDicomTag = DicomTagDictionary.GetDicomTag(stopTag) ??
			                        new DicomTag(stopTag, "Bogus Tag", "BogusTag", DicomVr.NONE, false, 1, 1, false);
			Load(stopDicomTag, options);
		}

		/// <summary>
		/// Load a DICOM file (as set by the <see cref="Filename"/> property).
		/// </summary>
		/// <remarks>
		/// Note:  If the file does not contain DICM encoded in it, the routine will assume
		/// the file is not a Part 10 format file, and is instead encoded as just a DataSet
		/// with the transfer syntax set to Implicit VR Little Endian.
		/// </remarks>
		/// <param name="stopTag"></param>
		/// <param name="options">The options to use when reading the file.</param>
		/// <param name="filename">The path of the file to load.</param>
		void Load(DicomTag stopTag, DicomReadOptions options, String filename)
		{
			Platform.CheckForEmptyString(filename, "filename");
			Filename = filename;
			Load(stopTag, options);
		}

		/// <summary>
		/// Load a DICOM file (as set by the <see cref="Filename"/> property).
		/// </summary>
		/// <remarks>
		/// Note:  If the file does not contain DICM encoded in it, the routine will assume
		/// the file is not a Part 10 format file, and is instead encoded as just a DataSet
		/// with the transfer syntax set to Implicit VR Little Endian.
		/// </remarks>
		/// <param name="stopTag"></param>
		/// <param name="options">The options to use when reading the file.</param>
		void Load(DicomTag stopTag, DicomReadOptions options)
		{
			var streamOpener = StreamOpener;
			Platform.CheckForNullReference(streamOpener, "filename"); // the only reason why stream opener is null here is because filename is empty
			using (var stream = streamOpener.Open())
				LoadCore(stream, streamOpener, stopTag, options);
		}

		/// <summary>
		/// Load a DICOM file from an input stream.
		/// </summary>
		/// <remarks>
		/// Note:  If the file does not contain DICM encoded in it, and
		/// <see cref="Stream.CanSeek"/> is true for <paramref name="stream"/>,
		/// the routine will assume the file is not a Part 10 format file, and is
		/// instead encoded as just a DataSet with the transfer syntax set to
		/// Implicit VR Little Endian.
		/// </remarks>
		/// <param name="stream">The input stream to read from.</param>
		void Load(Stream stream)
		{
			const DicomReadOptions options = DicomReadOptions.Default;
			Platform.CheckForNullReference(stream, "stream");
			LoadCore(stream, null, null, options);
		}

		/// <summary>
		/// Load a DICOM file from an input stream, given a delegate to open the stream.
		/// </summary>
		/// <remarks>
		/// Note:  If the file does not contain DICM encoded in it, and
		/// <see cref="Stream.CanSeek"/> is true for the stream returned by <paramref name="streamOpener"/>,
		/// the routine will assume the file is not a Part 10 format file, and is
		/// instead encoded as just a DataSet with the transfer syntax set to
		/// Implicit VR Little Endian.
		///
		/// Also, if you are using the <see cref="DicomReadOptions.StorePixelDataReferences"/> option with
		/// a <see cref="Stream"/> as opposed to simply a file name, you must use this method so that the
		/// stream can be reopenened internally whenever pixel data is accessed.
		/// </remarks>
		/// <param name="streamOpener">A delegate that opens the stream to read from.</param>
		/// <param name="stopTag">The dicom tag to stop the reading at.</param>
		/// <param name="options">The dicom read options to consider.</param>
		void Load(File streamOpener, DicomTag stopTag, DicomReadOptions options)
		{
			Platform.CheckForNullReference(streamOpener, "streamOpener");
			StreamOpener = streamOpener;
			using (var stream = streamOpener.Open())
				LoadCore(stream, streamOpener, stopTag, options);
		}

		/// <summary>
		/// Load a DICOM file from an input stream.
		/// </summary>
		/// <remarks>
		/// Note:  If the file does not contain DICM encoded in it, and
		/// <see cref="Stream.CanSeek"/> is true for <paramref name="stream"/>,
		/// the routine will assume the file is not a Part 10 format file, and is
		/// instead encoded as just a DataSet with the transfer syntax set to
		/// Implicit VR Little Endian.
		///
		/// Also, this overload cannot be used directly with <see cref="DicomReadOptions.StorePixelDataReferences"/>,
		/// as there must be a way to re-open the same stream at a later time. If the option is required,
		/// use the <see cref="Load(Func{Stream}, DicomTag, DicomReadOptions)">overload</see> that accepts a delegate for opening the stream.
		/// </remarks>
		/// <param name="stream">The input stream to read from.</param>
		/// <param name="stopTag">The dicom tag to stop the reading at.</param>
		/// <param name="options">The dicom read options to consider.</param>
		void Load(Stream stream, DicomTag stopTag, DicomReadOptions options)
		{
			Platform.CheckForNullReference(stream, "stream");
			LoadCore(stream, null, stopTag, options);
		}
*/
/*
		void _LoadCore(Stream stream, File streamOpener,
		               DicomTag stopTag, DicomReadOptions options) {
			// TODO CR (24 Jan 2014): DICOM stream read only uses tag value, so
		  // the real implementation should be the int overload!
			if (stopTag == null)
				stopTag = new DicomTag(0xFFFFFFFF, "Bogus Tag", "BogusTag", VR.NONE, false, 1, 1, false);

			DicomStreamReader dsr;

			var iStream = stream ?? streamOpener.Open();
			if (iStream.CanSeek) {
				iStream.Seek(128, SeekOrigin.Begin);
				if (!FileHasPart10Header(iStream)) {
					if (!Flags.IsSet(options, DicomReadOptions.ReadNonPart10Files))
						throw new DicomException("File is not part 10 format file: $_filename");

					iStream.Seek(0, SeekOrigin.Begin);
					dsr = new DicomStreamReader(iStream)
					      	{
					      		streamOpener = _file,
					      		transferSyntax = UID.ImplicitVRLittleEndianDefaultTransferSyntaxforDICOM,
					      		_dataset = DataSet
					      	};
					DicomReadStatus stat = dsr.Read(stopTag, options);
					if (stat != DicomReadStatus.Success) {
						Platform.Log(LogLevel.Error, "Unexpected error when reading file: {0}", Filename);
						throw new DicomException("Unexpected read error with file: " + Filename);
					}

					TransferSyntax = TransferSyntax.ImplicitVrLittleEndian;
					if (DataSet.Contains($SopClassUid))
						MediaStorageSopClassUid = DataSet[$SopClassUid].ToString();
					if (DataSet.Contains($SopInstanceUid))
						MediaStorageSopInstanceUid = DataSet[$SopInstanceUid].ToString();

					Loaded = true;
					return;
				}
			}
			else
			{
				// TODO CR (04 Apr 2014): this code here is almost identical to the seekable stream above, except that we use the 4CC wrapper
				// we can combine these two when we trust that the wrapper works in all cases
				iStream = FourCcReadStream.Create(iStream);

				// Read the 128 byte header first, then check for DICM
				iStream.SeekEx(128, SeekOrigin.Begin);

				if (!FileHasPart10Header(iStream))
				{
					if (!Flags.IsSet(options, DicomReadOptions.ReadNonPart10Files))
						throw new DicomException(String.Format("File is not part 10 format file: {0}", Filename));

					iStream.Seek(0, SeekOrigin.Begin);
					dsr = new DicomStreamReader(iStream)
					      	{
					      		StreamOpener = streamOpener,
					      		TransferSyntax = TransferSyntax.ImplicitVrLittleEndian,
					      		Dataset = DataSet
					      	};
					DicomReadStatus stat = dsr.Read(stopTag, options);
					if (stat != DicomReadStatus.Success)
					{
						Platform.Log(LogLevel.Error, "Unexpected error when reading file: {0}", Filename);
						throw new DicomException("Unexpected read error with file: " + Filename);
					}

					TransferSyntax = TransferSyntax.ImplicitVrLittleEndian;
					if (DataSet.Contains($SopClassUid))
						MediaStorageSopClassUid = DataSet[$SopClassUid].ToString();
					if (DataSet.Contains($SopInstanceUid))
						MediaStorageSopInstanceUid = DataSet[$SopInstanceUid].ToString();

					Loaded = true;
					return;
				}
			}

			dsr = new DicomStreamReader(iStream)
			      	{
			      		TransferSyntax = TransferSyntax.ExplicitVrLittleEndian,
			      		StreamOpener = streamOpener,
			      		Dataset = _fmi
			      	};

			DicomReadStatus readStat =
				dsr.Read(new DicomTag(0x0002FFFF, "Bogus Tag", "BogusTag", DicomVr.UNvr, false, 1, 1, false), options);
			if (readStat != DicomReadStatus.Success)
			{
				Platform.Log(LogLevel.Error, "Unexpected error when reading file Meta info for file: {0}", Filename);
				throw new DicomException("Unexpected failure reading file Meta info for file: " + Filename);
			}

			MetaInfoFileLength = dsr.EndGroupTwo + 128 + 4;

			dsr.Dataset = DataSet;
			dsr.TransferSyntax = TransferSyntax;
			readStat = dsr.Read(stopTag, options);
			if (readStat != DicomReadStatus.Success)
			{
				Platform.Log(LogLevel.Error, "Unexpected error ({0}) when reading file at offset {2}: {1}", readStat, Filename, dsr.BytesRead);
				throw new DicomException("Unexpected failure (" + readStat + ") reading file at offset " + dsr.BytesRead + ": " + Filename);
			}

			Loaded = true;
		}

		/// Internal routine to see if the file is encoded as a DICOM Part 10 format file.
		///
		/// [fs] is the [File] being read.
		/// Returns true if the file has a DICOM Part 10 format file header.</returns>
		static bool FileHasPart10Header(File fs) {
		  //TODO finish

			return (!(fs.ReadByte() != (byte) 'D' ||
			          fs.ReadByte() != (byte) 'I' ||
			          fs.ReadByte() != (byte) 'C' ||
			          fs.ReadByte() != (byte) 'M'));

		}

		/// <summary>
		/// Save the file as a DICOM Part 10 format file (as set by the <see cref="Filename"/> property) with
		/// the default <see cref="DicomWriteOptions"/>.
		/// </summary>
		/// <returns>true on success, false on failure.</returns>
		bool Save() {
			return Save(DicomWriteOptions.Default);
		}
*/
		/// <summary>
		/// Save the file as a DICOM Part 10 format file (as set by the <see cref="Filename"/> property).
		/// </summary>
		/// <param name="options">The options to use when saving the file.</param>
		/// <returns></returns>
		bool Save(DicomWriteOption option) {
			String dirPath = Path.dirname(_filename);
			if (!((dirPath == null) || (dirPath == ""))) {
		     Directory dir = new Directory(dirPath);
				if (! dir.existsSync()) dir.createSync();
			}

			File file = new File(_filename);
			bool b = Save(option);
				//file.flush();
				//file.close();
				return b;
		}

/*
		/// <summary>
		/// Save the file as a DICOM Part 10 format file with the default <see cref="DicomWriteOptions"/>.
		/// </summary>
		/// <returns>true on success, false on failure.</returns>
		bool Save(String file)
		{
			if (file == null) throw new ArgumentNullException("file");
			Filename = file;
			return Save(DicomWriteOptions.Default);
		}

		/// <summary>
		/// Save the file as a DICOM Part 10 format file.
		/// </summary>
		/// <param name="options">The options to use when saving the file.</param>
		/// <param name="file">The path of the file to save to.</param>
		/// <returns></returns>
		bool Save(String file, DicomWriteOptions options)
		{
			if (file == null) throw new ArgumentNullException("file");

			Filename = file;

			using (FileStream fs = File.Create(Filename))
			{
				bool b = Save(fs, options);
				fs.Flush();
				fs.Close();
				return b;
			}
		}

		/// <summary>
		/// Save the file as a DICOM Part 10 format file.
		/// </summary>
		/// <param name="options">The options to use when saving the file.</param>
		/// <param name="iStream">The <see cref="Stream"/> to Save the DICOM file to.</param>
		/// <returns></returns>
		bool Save(Stream iStream, DicomWriteOptions options)
		{
			if (iStream == null) throw new ArgumentNullException("iStream");

			// Original code has seek() here, but there may be use cases where
			// a user wants to add the file into a stream (that may contain other data)
			// and the seek would cause the method to not support that.
			byte[] prefix = new byte[128];
			iStream.Write(prefix, 0, 128);

			iStream.WriteByte((byte) 'D');
			iStream.WriteByte((byte) 'I');
			iStream.WriteByte((byte) 'C');
			iStream.WriteByte((byte) 'M');

			DicomStreamWriter dsw = new DicomStreamWriter(iStream);
			dsw.Write(TransferSyntax.ExplicitVrLittleEndian,
			          _fmi, options | DicomWriteOptions.CalculateGroupLengths);

			MetaInfoFileLength = iStream.Position;

			dsw.Write(TransferSyntax, DataSet, options);

			iStream.Flush();

			return true;
		}
*/

  /// Method to dump the contents of a file to a StringBuilder object.
  void Dump(StringBuffer sb, String prefix, DicomDumpOption options) {
    //TODO finish
    //if (sb == null) throw new NullReferenceException("sb");
    sb.write(prefix);
    sb.writeln("File: " + _filename);
    sb.writeln();
    sb.write(prefix);
    sb.write("MetaInfo:");
    sb.writeln();
    //TODO finish
    //_fmi.Dump(sb, prefix, options);
    sb.writeln();
    sb.write(prefix);
    sb.write("DataSet:");
    sb.writeln();
    //TODO finish
    //DataSet.Dump(sb, prefix, options);
    sb.writeln();
  }
}
	/// Used to buffer the first 132 bytes of an unseekable DICOM stream, so that
	/// we can reset if it turns out to not be a Part 10 file
/*
  class _FourCcReadStream : Stream	{
		Stream _realStream;
		Stream _prefixStream;
		long _position;

			static FourCcReadStream Create(Stream realStream)
			{
				var prefixBuffer = new byte[132]; // 128 prefix + 4CC

				var bytesRead = 0;
				while (bytesRead < 132)
				{
					// fill the entire buffer - if Read returns 0, then we encountered an EOF and the stream is definitely not a part 10 file, but might still be valid!
					var read = realStream.Read(prefixBuffer, bytesRead, prefixBuffer.Length - bytesRead);
					if (read == 0) break;
					bytesRead += read;
				}

				return new FourCcReadStream(new MemoryStream(prefixBuffer, 0, bytesRead, false), realStream);
			}

			private FourCcReadStream(Stream prefix, Stream realStream)
			{
				_prefixStream = prefix;
				_realStream = realStream;
				_position = 0;
			}

			override void Dispose(bool disposing)
			{
				if (!disposing) return;

				if (_realStream != null)
				{
					_realStream.Dispose();
					_realStream = null;
				}

				if (_prefixStream != null)
				{
					_prefixStream.Dispose();
					_prefixStream = null;
				}
			}

			override bool CanRead
			{
				get { return true; }
			}

			override bool CanSeek
			{
				get { return false; }
			}

			override bool CanWrite
			{
				get { return false; }
			}

			override long Length
			{
				get { return _realStream.Length; }
			}

			override long Position
			{
				get { return _position; }
				set { throw new NotSupportedException(); }
			}

			override int Read(byte[] buffer, int offset, int count)
			{
				var bytesRead = (_position < _prefixStream.Length ? _prefixStream : _realStream).Read(buffer, offset, count);
				_position += bytesRead;
				return bytesRead;
			}

		 int ReadByte() {
				var result = (_position < _prefixStream.Length ? _prefixStream : _realStream).ReadByte();
				if (result >= 0) ++_position;
				return result;
			}

		 long Seek(long offset, SeekOrigin origin) {
				if (_position <= _prefixStream.Length && offset == 0 && origin == SeekOrigin.Begin)
					return _position = _prefixStream.Position = 0;
				throw new InvalidOperationException("Unable to reset stream when the position has already advanced past the prefix");
			}

		 void Write(byte[] buffer, int offset, int count) {
				throw new NotSupportedException();
			}

			 void Flush() {
				throw new NotSupportedException();
			}

			void SetLength(long value) {
				throw new NotSupportedException();
			}

  }

   */


