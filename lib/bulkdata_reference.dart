// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library bulkdata_reference;

import 'package:dictionary/vr.dart';

 /**
  * BulkdataReference Object
  *
  * Note: this is the value associated with the BulkdataReference Attribute above.
  */
 class BDReference {
   final VR   vr;
   final int  offset;
   final int  length;
   final Uri  uri;

   BDReference(this.vr, this.offset, this.length, this.uri);

   String toString() => "BulkdataReference($this.hashCode)";
 }
