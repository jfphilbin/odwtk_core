// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library MRN;

class IssuerOfMRN {
  String   name;
  int      _counter = 1000;
  String   _id;

  String  get id => _id;

  /// Used to create existing IDs
  IssuerOfMRN(this.name, this._id) {
    issuers[this.name] = this;
  }

  /// Used to generate new IDs
  IssuerOfMRN.next(String issuerName) {
    name = issuerName;
    _counter++;
    _id  = _counter.toString();
  }

  static Map<String, IssuerOfMRN> issuers = new Map();

  String toString() => "IssuerOfMRN($this.hashCode)";
}

class MRN {
  IssuerOfMRN _issuer;
  int         _counter = 1000;
  String      _id;

  IssuerOfMRN get issuer => _issuer;

  String      get id     => _id;

  // Used for existing IDs
  MRN(this._issuer, this._id);


  // Used to generate new IDs
  MRN.next(IssuerOfMRN issuer) {
    issuer = _issuer;
    _counter++;
    _id  = _counter.toString();
  }
}

