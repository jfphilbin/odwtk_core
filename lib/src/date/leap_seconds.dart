// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library leap_seconds;

import 'dart:core' hide DateTime;
import 'package:core/src/date/date_time.dart';

//TODO Implement a timed reminder on startup that requests that this table be
//     updated if the end of the months of December 31, March 31, June 30, or
//     September 30 has passed since the last update.

/**
 * A class containing _leap seconds_.
 *
 * "A leap second is a one-second adjustment that is occasionally applied
 *  to Coordinated Universal Time (UTC) in order to keep its time of day
 *  close to the mean solar time. Without such a correction, time reckoned
 *  by Earth rotation drifts away from atomic time because of irregularities
 *  in the Earth's rate of rotation. Since this system of correction was
 *  implemented in 1972, 25 such leap seconds have been inserted."
 *  -- Wikipedia
 *  See <http://en.wikipedia.org/wiki/Leap_second#Proposal_to_abolish_leap_seconds>.
 *
 * Leap seconds when added are added at the end of the last day of December,
 * March, June, or September.  December and June are preferred, and so far leap
 * seconds have only been added at the end of December or June.  The leap second
 * that was added in June of 1972 would be represented in DICOM as "1920630235960".
 * Leap seconds are the only seconds that have the number 60.
 *
 * Futher reading:
 *   1. A table of leap seconds is kept at: <http://maia.usno.navy.mil/ser7/tai-utc.dat>
 *   2. <http://tycho.usno.navy.mil/leapsec.html>
 *   3. <http://www.timeanddate.com/time/leapseconds.html>
 */
class LeapSecond {
  //*** When updating this class, please update the [revisionDate]
  final String revisionDate = "25 July 2014";
  final int year, month, day;

  const LeapSecond(this.year, this.month, this.day);

  static const June1972     = const LeapSecond(1972,  6, 30);
  static const December1972 = const LeapSecond(1972, 12, 31);
  static const December1973 = const LeapSecond(1973, 12, 31);
  static const December1974 = const LeapSecond(1974, 12, 31);
  static const December1975 = const LeapSecond(1975, 12, 31);
  static const December1976 = const LeapSecond(1976, 12, 31);
  static const December1977 = const LeapSecond(1977, 12, 31);
  static const December1978 = const LeapSecond(1978, 12, 31);
  static const December1979 = const LeapSecond(1979, 12, 31);
  static const June1981     = const LeapSecond(1981,  6, 30);
  static const June1982     = const LeapSecond(1982,  6, 30);
  static const June1983     = const LeapSecond(1983,  6, 30);
  static const June1985     = const LeapSecond(1985,  6, 30);
  static const December1987 = const LeapSecond(1987, 12, 31);
  static const December1989 = const LeapSecond(1989, 12, 31);
  static const December1990 = const LeapSecond(1990, 12, 31);
  static const June1992     = const LeapSecond(1992,  6, 30);
  static const June1993     = const LeapSecond(1993,  6, 30);
  static const June1994     = const LeapSecond(1994,  6, 30);
  static const December1995 = const LeapSecond(1995, 12, 31);
  static const June1997     = const LeapSecond(1997,  6, 30);
  static const December1998 = const LeapSecond(1998, 12, 31);
  static const December2005 = const LeapSecond(2005, 12, 31);
  static const December2008 = const LeapSecond(2008, 12, 31);
  static const June2012     = const LeapSecond(2012,  6, 30);

  /// A [Map] containing the leap seconds that have been issued so far.
  /// Its key is created by multiplying the year times the month as integer,
  /// i.e. [key] = [int year] * [int month].
  static const Map leapSecondMap
      = const { 06*1972: June1972,
                12*1972: December1972,
                12*1973: December1973,
                12*1974: December1974,
                12*1975: December1975,
                12*1976: December1976,
                12*1977: December1977,
                12*1978: December1978,
                12*1979: December1979,
                06*1981: June1981,
                06*1982: June1982,
                06*1983: June1983,
                06*1985: June1985,
                12*1987: December1987,
                12*1989: December1989,
                12*1990: December1990,
                06*1992: June1992,
                06*1993: June1993,
                06*1994: June1994,
                12*1995: December1995,
                06*1997: June1997,
                12*1998: December1998,
                12*2005: December2005,
                12*2008: December2008,
                06*2012: June2012
              };

  /// Lookup a _leap second_ using the [year] and [month] it was issued.
  /// Returns [null] if no _leap second_ found.
  static LeapSecond lookup(int year, int month) => leapSecondMap[year*month];

  /// Returns [true] if the second represented by y:m:d:h:mn:s is a _leap second_,
  /// [false] otherwise.
  static bool isValid(int y, int m, int d, int h, int mn, int s) {
    LeapSecond ls = leapSecondMap[y*m];
    if (ls == null) return false;
    if ((y == ls.year) && (m == ls.month) && (d == ls.day) &&
        (h == 23) && (mn == 59) && (60 == s))
      return true;
    else return false;
  }

  /// Returns the [LeapSecond] instance as a DICOM [DateTime].
  DateTime get dateTime => new DateTime(year, month, day, 23, 59, 60);

/*
  /// Returns the [LeapSecond] instance as a Dart [DateTime].
  DartDateTime dartDateTime() => new DartDateTime(year, month, day, 23, 59, 60);
*/
  toString() => 'LeapSecond for $month/$year';
}