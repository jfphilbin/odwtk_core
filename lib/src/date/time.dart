// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library time;

//import 'package:core/src/date/date_marks.dart';
import 'package:core/src/date/date_time_utils.dart' as dtu;
import 'package:core/src/date/time_zone.dart';

//TODO document: unspecified units are null?
class Time {
  int _h, _m, _s, _us;
  TimeZone _tz;
  String   _dMark = '.';  // Decimal mark

  // Constructor
  Time(this._h, [this._m, this._s, this._us, this._tz]) {
    if (_us != null) dtu.isValidMicrosecond(_h, _m, _s, _us);
    else if (_s != null) dtu.isValidSecond(_h, _m, _s);
    else if (_m != null) dtu.isValidMinute(_h, _m);
    else dtu.isValidHour(_h);
  }

  // Getters
  int get hour        => _h;
  int get minute      => _m;
  int get second      => _s;
  int get microsecond => _us;

  String get _hStr     => dtu.hStr(_h);
  String get _mStr     => dtu.mStr(_m);
  String get _sStr     => dtu.sStr(_s);
  String get _usStr    => dtu.usStr(_dMark, _us);
  String get _tzStr    => dtu.tzStr(_tz);
  String get _tzExtStr => (_tz == null) ? '' : '${_tz.httpString}';

  String get dicomString => '$_hStr$_mStr$_sStr$_usStr$_tzStr';

  //TODO this could be more efficient
  String get httpString {
    if ((_m == null) && (_s == null) && (_us == null) && (_tz  == null)) return '$_hStr';
    if ((_s == null) && (_us == null) && (_tz  == null)) return '$_hStr:$_mStr';
    if ((_us == null) && (_tz  == null)) return '$_hStr:$_mStr:$_sStr';
    if ((_tz  == null)) return '$_hStr:$_mStr:$_sStr$_usStr';
    return '$_hStr:$_mStr:$_sStr$_usStr$_tzStr';
  }

  String get decimalMark => _dMark;
  void   set decimalMark(String m) {
    ((m == '.') || (m == ',')) ? _dMark = m : _error('invalid Decimal Mark: $m');
  }

//TODO remove when working
/*
  /// Parse a valid DICOM date. WARNING: No error checking!
  static DcmTime parseString(String str, [start = 0, end]) {
    int h, m, s, us;
    if (end == null) end = str.length;
    if ((end - start) > 16) _error('Time string=$str too long');
    int len = end - start;
    if (len >= 2) h = stringToInt(str, start, start + 2, 0, 23);
    if (len >= 4) m = stringToInt(str, start + 2, start + 4, 0, 59);
    if (len >= 6) s = stringToInt(str, start + 4, start + 6, 0, 60);
    if (len > 6) {
      int i = start + 6;
      print('point=${str[i]}');
      if ((str[i] != Ascii.PERIOD) || (str[i] != Ascii.COMMA))
        _error('invalid character where decimal indicator should be');
      us = stringToInt(str, start + 7, end, 0, 999999);
    }
    return new DcmTime(h, m, s, us);
    //} else {
    //  return _error('Invalid DateDcm:$str');
    //}
  }

  // Read a positive integer (radix 10) of exact length nChars.
  // Trailing space is allowed, but leading and embedded space is illegal.
  static int stringToInt(String s, int start, int end, int min, int max) {
    if (s[start] == Ascii.SPACE) {
      for(int i = start+1; i < end; i++) {
        if (s[i] != Ascii.SPACE) _error('illegal embedded space found');
      }
    }
    int n = 0;
    for(int i = start; i < end; i++) {
      int char = s[i].codeUnitAt(0);
      if ((char >= Ascii.DIGIT_0) && (char <= Ascii.DIGIT_9)) {
        n = (n * 10) + (char - Ascii.DIGIT_0);
      } else _error('invalid CodeUnit= $char');
    }
    if ((n < min) || (n > max)) _error('value out of range');
    return n;
  }

  /// Parse a Uint8List into a valid DICOM date, and return it.
  /// The maximum length of the string is 16. The string may be padded with trailing
  /// spaces. Leading and embedded spaces are not allowed.
  static DcmTime parseBytes(Uint8List bytes, [int start=0, end]) {
    int h, m, s, us;
    if (end == null) end = bytes.length;
    if ((end - start) > 16) _error('Time string=$bytes too long');
    int len = end - start;
    if (len >= 2) h = bytesToInt(bytes, start, start + 2, 0, 23);
    if (len >= 4) m = bytesToInt(bytes, start + 2, start + 4, 0, 59);
    if (len >= 6) s = bytesToInt(bytes, start + 4, start + 6, 0, 60);
    if (len > 6) {
      int i = start + 6;
      print('dec point = ${bytes[i]}');
      if ((bytes[i] != Ascii.PERIOD) || (bytes[i] != Ascii.COMMA))
        _error('invalid character where decimal indicator should be');
      us = bytesToInt(bytes, start + 7, end, 0, 999999);
    }
    return new DcmTime(h, m, s, us);
    //} else {
    //  return _error('Invalid DateDcm:$bytes');
  }

  /// Parse a positive integer (radix 10) of exact length = end - start,
  /// and return its value.
  static int bytesToInt(Uint8List bytes, int start, int end, int min, int max) {
    if (bytes[start] == Ascii.SPACE) {
      for(int i = start+1; i < end; i++) {
        if (bytes[i] != Ascii.SPACE) _error('illegal embedded space found');
      }
    }
    int n = 0;
    for(int i = start; i < end; i++) {
      int char = bytes[i];
      if ((char >= Ascii.DIGIT_0) && (char <= Ascii.DIGIT_9)) {
        n = (n * 10) + (char - Ascii.DIGIT_0);
      } else if (char == Ascii.SPACE) {
        _error('invalid whitespace');
      } else _error('invalid CodeUnit= $char');
    }
    //Note: this test is duplicated in the constructor.
    if ((n < min) || (n > max)) _error('value out of range');
    return n;
  }
*/
  Time _error(msg) => throw new ArgumentError(msg);

  void debug() {
    print('Time:[h=$_h, m=$_m, s=$_s, us=$_us, tz=$_tz]');
    print('\tB=\"$dicomString\", E=\"$httpString\"');
  }

  toString() => httpString;
}