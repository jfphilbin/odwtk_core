// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library date_time_format;

import 'package:utilities/utilities.dart';
import 'package:core/src/date/date_time_utils.dart' as dtu;

/**
 * [this] is used to specify the formats allowed when reading and writing [Date]s,
 * [Times], and [DateTime]s.  The formats are base on the ISO 8601.2004(E) standard.
 * contains information that is used to control reading and writing
 *
 * For further information see: <./date_time.md>
 *
 */

//TODO finish

// Parameters:
//   0. Decimal point:                   String decimalMark = "."
//   1. Basic/Extended:                  bool isExtended false
//   2. Reduced precision allowed:       bool reducedPrecisionAllowed false
//   3. Expanded representation allowed: bool expansionAllowed false
//   3. Time mark allows space:          bool spaceAsTimeMark false
//
// Note: This package can't use Dart's [int.parse] because it allows leading
//       and trailing whitespace!

//TODO add support for years < 0001

class DateTimeFormat {
  final bool allowBasicFormat;
  final bool allowExtendedFormat;
  final bool allowReducedAccuracy;
  final bool allowExpandedYearRange;
  final bool allowSpaceAsTimeMark;
        bool _allowSignedYear;
        int  _firstYear;
        int  _lastYear;
        int  _decimalMark;

  DateTimeFormat([firstYear = 1582,
                  lastYear  = 9999,
                  //TODO remove next line when final
                  //this._decimalMark,
                  this.allowBasicFormat     = true,
                  this.allowExtendedFormat  = false,
                  this.allowReducedAccuracy = false,
                  this.allowExpandedYearRange = false,
                  this.allowSpaceAsTimeMark = false]) {
    _allowSignedYear = setYearRange(firstYear, lastYear);
  }

   factory DateTimeFormat.args(
      {firstYear:                   1582,
       lastYear:                    9999,
       decimalMark:                 $_PERIOD,
       allowBasicFormat:            true,
       allowExtendedFormat: false,
       allowReducedAccuracy:        false,
       allowSpaceAsTimeMark:       false,
       isYearRangeExpansionAllowed: false}) {
    //_allowSignedYear = setYearRange(firstYear, lastYear);
    return new DateTimeFormat(firstYear, lastYear,
                              //TODO remove when final
                              //decimalMark,
                              allowBasicFormat,
                              allowExtendedFormat,
                              allowReducedAccuracy,
                              allowSpaceAsTimeMark);
  }

  bool   get signedYearAllowed => _allowSignedYear;
  int    get firstYear   => _firstYear;
  int    get lastYear    => _lastYear;
  //int    get decimalMark => _decimalMark;
  //String get decimalMarkAsString => dtu.charToString(_decimalMark);

  void set firstYear(year) {
    if (0000 < year)  _firstYear = year;
    else throw new RangeError('firstYear less than 0001 not currently supported');
  }
  void set lastYear(year) {
    if (year < 10000)  _lastYear = year;
    else throw new RangeError('lastYeargreater than 9999 not currently supported');
  }

  void set decimalMark(int mark) {
    if ((mark != $_PERIOD) && (mark != $_COMMA))
        throw new ArgumentError('Decimal Marks can only be \".\" or \",\".');
    _decimalMark = mark;
  }

  // Returns [true] if Years are allowed to be negative; otherwise, returns false;
  bool setYearRange(int first, int last) {
    if (allowExpandedYearRange) {
      firstYear = first;
      lastYear  = last;
      return (firstYear < 0) ? true : false;
    } else
      dtu.error('This Date/Time format does not allow Expanded Year ranges.');
    return false;
  }

  //TODO Are there other we should add?
  static final BASIC = new DateTimeFormat();
  static final DICOM = BASIC;
// static final HTTP = new DateTimeFormat.args(allowExtendedFormat: true);

}


