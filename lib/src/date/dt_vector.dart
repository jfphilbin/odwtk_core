// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library dt_vector;

import 'dart:core' hide DateTime;
import 'package:core/src/date/date.dart';
import 'package:core/src/date/time.dart';
import 'package:core/src/date/date_time.dart';
import 'package:core/src/date/time_zone.dart';

// This is a class used to collect the date/time information as it is recursively
// parsed.  It is a private class and should never excape.
//TODO implement this more efficiently using Uint8List
class DT {
  int type;
  int y, m, d, h, mn, s, us, tzs, tzh, tzm; // year, month, day...

  static const _DATE = 0;
  static const _TIME = 1;
  static const _DATE_TIME = 2;

  // Constructors
  DT.forDate()     { type = _DATE; }
  DT.forTime()     { type = _TIME; }
  DT.forDateTime() { type = _DATE_TIME; }

  bool get isDate     => type == _DATE;
  bool get isTime     => type == _TIME;
  bool get isDateTime => type == _DATE_TIME;

  Date     get date     => new Date(y, m, d);
  Time     get time     => new Time(h, mn, s, us, timeZone);
  DateTime get dateTime => new DateTime(y, m, d, h, mn, s, us, timeZone);
  TimeZone get timeZone => (tzs == null) ? null : new TimeZone(tzs, tzh, tzm);
}
