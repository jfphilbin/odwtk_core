// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library date_time_reader;

import 'dart:core' hide DateTime;
import 'dart:typed_data';
import 'dart:convert';

import 'package:core/src/date/date.dart';
import 'package:core/src/date/date_time.dart';
import 'package:core/src/date/date_time_format.dart';
import 'package:core/src/date/date_time_utils.dart' as dtu;
import 'package:core/src/date/dt_vector.dart';
import 'package:core/src/date/leap_seconds.dart';
import 'package:core/src/date/time.dart';
import 'package:core/src/date/time_zone.dart';
import 'package:utilities/utilities.dart';

/**
 * This package contains a class for reading dates, times, and dateTimes in
 * ISO 8601.2000(E) in both Basic and Extended formats.  It supports both Full
 * and Reduced accuracy. It supports Expanded representations in which the First
 * and Last Years inclusive are specified by the user. It supports both Ascii
 * Period '.' and Ascii Comma ',' as decimal signs and it allows Ascii Space ' '
 * to be used as a Time designator.
 *
 * It DOES NOT support Hours fields with the number 24 and does not recognize
 * 24:00:00 as midnight.  It DOES NOT support Truncated representations.
 *
 * It also includes suport for DICOM Standard and IETF RFC3339 profiles of ISO
 * 8602.2000(E)
 *
 * DICOM Support
 * DICOM Dates, Times and DateTimes are a profile of ISO 8601 that only supports
 * the Basic format with Full and Reduced Accuracy. The format for a DICOM
 * DateTime is YYYYMMDDHHMMSS.FFFFFF&ZZXX. The maximum length is 26 characters.
 * Trailing spaces are allowed, but leading and embedded spaces are illegal.
 *
 * A date or time component that is omitted from the string is termed a null
 * component.  DICOM allows trailing null components of Date, Time, and DateTime
 * to indicate that the value is not precise to the accuracy of those components.
 * The Year (YYYY) component can not be null in Date or DateTime formats, and the
 * Hour (HH) component can not be null in Time formats.  Non-trailing null
 * components are prohibited and generate errors. The optional time zone offset
 * suffix is not considered as a component, and may be in included Time and
 * DateTime even if there are null components.
 *
 * IETF RFC 3339 Support
//TODO finish
 */

// Design Notes:
//   1. Each date/time component reader (readYear, readMonth...) is responsible
//     for the following, in order:
//       a. before starting to parse the component, check that the [buffer] is
//          not empty and remove trailing whitespace.
//       b. read any marks that preceed the component, such as:
//            '+/-' - before year in Expanded representations of years that
//                  - include negative years
//            '-'   - preceeds month and day in date in Extended format
//            'T/t' - preceeds time or dateTime in Extended format
//            ':'   - preceeds minute or second in Extended format
//            './,' - preceeds fraction
//            'Z/z' - preceeds timezone in Extended format (with no following chars)
//            '+/-' - preceeds timezone hour offset
//       c. read at least the min number an up to the max number of chars into
//          a positive number.  If any other char is read before the min is reached,
//          an error should be thrown.
//       d. when reading the fraction any non-digit should check for trailing
//          whitespace, or trailing Z/z/+/-; otherwise an error is thrown.
//       e. a time zone can follow and time component
//       f. time zones start with Z/z which indicate the end of the date/time
//          and can only be followed by trailing whitespace; or +/- which must
//          be followed by at least a valid time zone hour component and a time
//          minute component, which is required in Basic format and optional in
//          extended format.
//       g. leading and embedded spaces throw errors
//       h. when an error is throw the [buffer][_index] should point to the first
//          char that caused the error.
//
// Parameters:
//   0. Decimal point:                   String decimalMark = "."
//   1. Basic/Extended:                  bool isExtended false
//   2. Reduced accuracy allowed:        bool reducedAccuracyAllowed false
//   3. Expanded representation allowed: bool expansionAllowed false
//   3. Time mark allows space:          bool spaceAsTimeMark false
//
// Note: This package can't use Dart's [int.parse] because it allows leading
//       and trailing whitespace!


/**
 * Reads [Date]s, [Time]s and [DateTime]s from a [String] or [Uint8List].
 *
 * See <./date_time.md> for details.
 *
 */
class DateTimeReader {
  DateTimeFormat _dtf = DateTimeFormat.DICOM;
  Uint8List        _buffer;     // The underlying buffer containing bytes
  int               _start = 0; // The position in the buffer corresponding to index=0
  int               _end;        // One more than the index of the last byte in this buffer
  int               _index;      // The current position in the buffer

  //TODO decide is localTimeZone needed here
  //TimeZone  _localTZ = TimeZone.getLocalTimeZone();

  // Getters for different time components
  bool get _empty      => (_index >= _end);
  int  get _readChar   => (_empty) ? _emptyBufferError('_readChar') : _buffer[_index++];
  int  get _unreadChar => _index--;
  int  get _remaining  => (_end - _index);
  //The next two are not used
  int  get _peekChar   => (_empty) ? _emptyBufferError('_peekChar') :_buffer[_index];
  //int  get _unpeekChar => _index++;

  // Constructors
  DateTimeReader(this._buffer, [this._start = 0, this._end]) {
    if (_end == null) _end = _buffer.length;
    _index = _start;
   dtu.debug('$_start, $_end');
  }

  factory DateTimeReader.fromString(String s, [int start = 0, int end]) {
    List buffer = s.codeUnits;
    return new DateTimeReader(buffer, start, end);
  }

  //****  Public Interface  *********************************************/

  /*
   * [this] is a getter that reads a [Date] in ISO 8601 format with an optional
   * time zone designator.  The second can optionally be follow by a decimal
   * mark and from 0 to 6 integers.
   *
   * Examples:
   *  Basic:    19890630,   19890630,    19890630.123-0500
   *  Extended: 1989:06:30, 1989:06:30,  1989:06:30.1-05:00
   *
   */
  Date get date => readDate();

  Date readDate() {
    DT dt = new DT.forDate();
    bool extended = _dtf.allowExtendedFormat;
    _readSign(dt, extended);
    //TODO should dates have timezones?
    _maybeReadTimeZone(dt, extended);
    return dt.date;
  }
  /*
   * [this] is a getter that reads a [Time] in ISO 8601 format with an optional
   * time zone designator.  The second can optionally be follow by a decimal
   * mark and from 0 to 6 integers. In Extended format a Time designator can
   * optionally preceed the hour component.
   *
   * Examples:
   *  Basic:    123059,     123059.123456,    123059.987-0500
   *  Extended: 12:30:59, T12:30:59.123456, 12:30:59.-05:00
   *
   */
  Time get time => readTime();

  Time readTime() {
    DT dt = new DT.forTime();
    bool extended = _dtf.allowExtendedFormat;
    // if (_dtf != DateTimeFormat.DICOM) _readMark($_LETTER_T, false);
    _readHour(dt, extended);
    _maybeReadTimeZone(dt, extended);
    return dt.time;
  }

  /*
   * [this] is a getter that reads a [DateTime] in ISO 8601 format with an
   * optional  time zone designator.  The second can optionally be follow
   * by a decimal mark and from 0 to 6 integers. In Extended format a Time
   * designator can optionally preceed the hour component.
   *
   * Examples:
   *           Basic                    Extended
   *       19890630123059          1989:06:30T12:30:59
   *       19890630123059.123456   1989:06:30T12:30:59.123456
   *       19890630123059.1-0500   1989:06:30T12:30:59.1-05:00
   *
   */
  DateTime get dateTime => readDateTime();

  DateTime readDateTime() {
    DT dt = new DT.forDateTime();
    bool extended = _dtf.allowExtendedFormat;
    _readSign(dt, extended);
    _maybeReadTimeZone(dt, extended);
    return dt.dateTime;
  }

  /*
   * [this] is a getter for reading Time Zones
   */
  TimeZone get timeZone => readTimeZone();

  TimeZone readTimeZone() {
    DT dt = new DT.forDateTime();
    bool extended = _dtf.allowExtendedFormat;
    _maybeReadTimeZone(dt, extended);
    return dt.timeZone;
  }

//*****  Private Methods - Date Auxilliaries ***********************************/

  // Returns 1 if a '+' is read, and -1 if a '-' is read; otherwise, it unreads
  // the character and return 1, assuming what follows is a positive number.
  void _readSign(DT dt, bool extended) {
    int sign;
    if (_dtf.signedYearAllowed) {
      int char = _readChar;
      // Read a plus '+' or minus '-' sign, if there.
      if (char == $_MINUS) sign = -1;
      else if (char == $_PLUS) sign = 1;
      else {
        _unreadChar;
        sign = 1;
      }
    } else sign = 1;
    dtu.debug('_readSign: _index=$_index');
    _readYear(dt, extended, sign);
  }

  void _readYear(DT dt, bool extended, int sign) {
    const msg = 'year is required in Date and DateTime';
    //TODO does the message above get thrown or something inside _isEmpty
    if (_isEmpty(false)) dtu.error(msg);
    //int sign = _readSign();
    int year = _readUnsignedInteger(4, 4, _dtf.firstYear, _dtf.lastYear);
    dt.y = sign * year;
    _readMonth(dt, extended);
  }



  // Returns after:
  //   1. reading a valid month number
  //   2. doing nothing because the buffer is empty or contains only trailing spaces
  void _readMonth(DT dt, bool extended) {
    if (_isEmpty(true)) return;
    //TODO use maybeReadDash if we're supporting Truncated format
    if (extended)  _readMark($_MINUS);
    dt.m  = _readUnsignedInteger(2, 2, 1, 12);
    _readDay(dt, extended);
  }

  void _readDay(DT dt, bool extended) {
    if (_isEmpty(true)) return;
    if (extended)  _readMark($_MINUS);
    dt.d = _readUnsignedInteger(2, 2, 1, dtu.daysInMonth(dt.y, dt.m));
    if (dt.isDate) return;
    else {
      _readTimeMark();
      _readHour(dt, extended);
    }
  }

//*****  Private Methods - Time Auxilliaries***********************************/

  void _readHour(DT dt, bool extended) {
    dtu.debug('rTime:extended=$extended');
    //The hour can be empty in a DateTime but not in a Time
    if (_isEmpty(dt.isDateTime)) return;
    if (_dtf != DateTimeFormat.DICOM) {
      bool wasRead = _readTimeMark();
      if (dt.isDateTime && (! wasRead))
        dtu.error('Time designator "T" is required when reading DateTime');
    }
    dt.h  = _readUnsignedInteger(2, 2, 0, 23);
    dtu.debug('rHour=$dt.h');
    _readMinute(dt, extended);
  }

  /**
   * Time Delimiter Mark = 'T'
   *
   * In DICOM format there is no time delimiter mark.  When reading
   * a Time it is optional.  When reading DateTime it is required.
   */
  bool _readTimeMark() =>  _readMark($_T);

  // Reads a TimeMark ('T' or 't') if it is the next char and returns [true].
  // If the next char is not a TimeMark returns [false].  If reader is empty
  // returns [null].  Time marks are only allowed in Extended format. They are
  // required for Extended format DateTime, but optional for Extended format Time.
 /* currently not used - but could be used for extension
  void _readOtherTimeMarks(DT dt, bool extended) {
   if (_isEmpty(dt.isTime)) return;
   int char = _readChar;
   if (((char == $_LETTER_T) || (char == $_LETTER_t)) ||
       (_dtf.allowSpaceAsTimeMark && (char == $_SPACE))) {
     return;
   }
   _unreadChar;
   return;
 }
*/

  void _readMinute(DT dt, bool extended) {
    dtu.debug('rMin');
    if (_isEmpty(true)) return;
    if (extended)  _readMark($_COLON);
    dt.mn = _readUnsignedInteger(2, 2, 0, 60);
    dtu.debug('rMin=$dt.mn');
    _readSecond(dt, extended);
  }

  //TODO this need explaination about months with 60 second and without 59 seconds.
  void _readSecond(DT dt, bool extended) {
    dtu.debug('rSec');
    if (_isEmpty(true)) return;
    if (extended)  _readMark($_COLON);
    int maxSecond = (dt.isTime) ? 59 : 58;
    dt.s = _readUnsignedInteger(2, 2, 0, maxSecond);
    dtu.debug('rSec=$dt.s');
    if (dt.isTime) return;
    if (! LeapSecond.isValid(dt.y, dt.m, dt.d, dt.h, dt.mn, dt.s))
      dtu.error('Invalid second value = $dt.s');
    _readMicrosecond(dt);
  }

  void _readMicrosecond(DT dt) {
    dtu.debug('rUsec');
    if (_isEmpty(true)) return;
    int char = _readChar;
    if ((char != $_PERIOD) && (char != $_COMMA)) return;
    // remove _readMark(_dtf.decimalMark);
    dt.us = _readUnsignedInteger(1, 6, 0, 999999);
    dtu.debug('rUsec=$dt.us');
   }

/*****  Time Zones ***********************************************/

  void _maybeReadTimeZone(DT dt, extended) {
    if (_isEmpty(false)) return;
    int char = _readChar;
    if (_dtf != DateTimeFormat.DICOM) {
      ///Check for UTC mark = "Z"
      if (char == $_Z) {
        dt.tzs = 1;
        dt.tzh = 0;
        dt.tzh = 0;
        return;
      }
    } else if (char == $_MINUS) {
      dt.tzs = -1;
    } else if (char == $_PLUS)  {
      dt.tzs =  1;
    } else {
      // No Time Zone
      _unreadChar;
      return;
    }
    //There is a sign, so read TZ hours.
    //if (dt.tzs == null)
    _readTZHour(dt, extended);
  }

  /// Reads a valid hour or throws an dtu.error.
  void _readTZHour(DT dt, bool extended) {
    if (_isEmpty(false)) dtu.error('Hour is a required field');
    // The TZ hour must be -14 <= hour <= 12
    dt.tzh = _readUnsignedInteger(2, 2, 0, 14);
    _readTZMinute(dt, extended);
  }

  /// TZ minutes are required in Basic format, but options in Extended format.
  void _readTZMinute(DT dt, extended) {
    bool isRequired = _dtf == DateTimeFormat.DICOM;
    if (_isEmpty(true)) {
      if (! isRequired) return;
      else dtu.error('Time Zone Minute field is required.');
    }
    // From here MUST read a valid minutes field (00 - 59), or throw an exception.
    if (extended) {
      if (_readMark($_COLON, false)) isRequired = true;
    }
    dt.tzm = _readUnsignedInteger(2, 2, 0, 59);
    if (isRequired && (dt.tzm == null))
        dtu.error('Time Zone Minutes required if ":" present');
    else return;
  }

//*****  Private Methods - Other Auxilliaries***********************************/

  /**
   * Read an unsigned integer [nChars] characters long in radix 10. The integer
   * must be greater than or equal to [min] and less than or equal to [max].
   * Trailing space is allowed, but leading and embedded space is illegal.
   * Returns [null] as the value of the integer if the [buffer] is empty;
   * otherwise, returns a valid integer or throws a [FormatException].
   *
   * Note: This method should never return null.  It should return a valid
   * integer or throw a FormatException
   *
   * */
  int _readUnsignedInteger(int minChars, int maxChars, int min, int max) {
    dtu.debug('rUInt');
    _checkRemaining(minChars);
    int n = 0;
    for(int i = 0; i <  maxChars; i++) {
      int char = _readChar;
      dtu.debug('rInt: char=\"${ASCII.decode([char])}\"');
      if ((char >= $_DIGIT_0) && (char <= $_DIGIT_9)) {
        n = (n * 10) + (char - $_DIGIT_0);
      } else {
        // A non-digit has been read
        if (i >= minChars) {
          dtu.debug('rUInt: int=$n, index=$_index');
          return _checkRange(n, min, max);
        } else {
            dtu.debug('rInt: char=\"${ASCII.decode([char])}\"');
            if (i < minChars) {
              String c = ASCII.decode([char]);
              dtu.error('Invalid char=$c at pos=$_index in _readUnsignedInteger.');
            }
        }
      }

    }
    return _checkRange(n, min, max);
  }

  int _checkRemaining(int n) {
    if (_remaining < n) dtu.error('Too few characters in buffer');
    return n;
  }

  int _checkRange(int value, int min, int max) {
    if ((value < min) || (value > max))
      dtu.error('value=$value out of range: $min < x < $max');
    return value;
  }

  // Returns [true] if [mark] (such as "-", "T", ":", "Z", "+", or "-")
  // matches the next [char] read.  If [mark] and [char] do not match, then
  // if [isRequired] == [true] throws an Exception (dtu.error); otherwise,
  // returns false.
 bool _readMark(int mark, [bool isRequired = true]) {
    int char = _readChar;
    if (char == mark) return true;
    else if (isRequired) {
            String ms = ASCII.decode([mark]);
            String cs = ASCII.decode([char]);
            dtu.error('Bad Mark char: read $cs when expecting $ms.');
    }
    return false;
  }

  /// Checks whether [_buffer] is empty. If the [allowReducedAccuracy]
  /// parameter is true and [reducedAccuracyOK] is [true], then returns
  /// [true]; otherwise, throws a FormatException;
  bool _isEmpty(bool reducedAccuracyOK) {
    dtu.debug('isEmpty: _index=$_index, _end=$_end, reduced=$reducedAccuracyOK');
    if ((_empty) || (reducedAccuracyOK && _dtf.allowReducedAccuracy)) return true;
    return false;
  }

  bool get _removeTrailingSpace {
    int char = _peekChar;
    dtu.debug('trailing peek =$char');
    if (char == $_SPACE) {
      for(int i = _index; i < _end; i++) {
        int c = _readChar;
        if (c!= $_SPACE) dtu.error('illegal embedded space found');
      }
      return true;
    };
    return false;
  }

  _emptyBufferError(caller) => dtu.error('Empty buffer caller: $caller');


  /*TODO not used but don't delete.

    /// Read a decimal fraction of at most [nChars].  If the [buffer] is empty or
    /// only contains trailing space returns [null].
    int _readFraction(int nChars, int min, int max) {

      if (_isEmpty(true)) return null;
      int char = _peekChar;
      dtu.debug('rFrac: char=\"${$_decode([char])}\"');
      //TODO should we allow '.' or ',' to be legal or should it be a parameter to reader?
      //     write now always prints '.'!
      if ((char != $_PERIOD) && (char != $_COMMA)) {
        dtu.debug('rFrac: No decimal mark');
        return null;
      } else {
        _incr;
        return _readUnsignedInteger(1, 6, min, max);
      }
    }
  */

}


