// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library date_time;

import 'dart:core' hide DateTime;
import 'dart:typed_data';

import 'package:core/src/date/date.dart';
import 'package:core/src/date/date_time_reader.dart';
import 'package:core/src/date/date_time_utils.dart' as dtu;
import 'package:core/src/date/dt_vector.dart';
import 'package:core/src/date/time.dart';
import 'package:core/src/date/time_zone.dart';

//TODO cleanup file and remove all unnecessary comments

/**
 * This package implements the DICOM DateTime Value Representation, and
 * follows the ISO 8601:2000 specification, where DICOM is silent.
 * The default Minimum Year is 1582, and the default Maximum Year is 9999,
 * as specified in ISO 8601:2000. However, this package has been tested with
 * Minimum Year of 0000. ISO 8601:2000 specifies that the Minimum and/or Maximum
 * years can be expanded by mutual agreement.
 *
 * DICOM DateTime has the format of YYYYMMDDHHMMSS.FFFFFF&ZZXX
 *
 * The maximum length is 26 characters.  Trailing spaces are allowed, but leading
 * and embedded spaces are illegal.
 *
 * A component that is omitted from the string is termed a null component.
 * Trailing null components of DateTime indicate that the value is not precise
 * to the precision of those components. The YYYY component can not be null.
 * Non-trailing null components are prohibited and generate errors. The optional
 * time zone offset suffix is not considered as a component.  Any component may be
 * null except the year.
 *
 * Note: The Time zoneOffset may be included even if there are null components.
 */

// Note: Can't use int.parse because it allows leading and trailing whitespace!

//TODO This could be optimized to be a ByteData object, which would be much smaller or
//     in microsecond since
//TODO whould this work better?
/*
DcmDate date;
DcmTime time;
DcmDateTime.other(int _y, [ int _m, int _d, int _h, int _mn, int _s, int _us, TimeZone_tz]) {
  DcmDate date = new DcmDate(_y, _m, _d);
  DcmTime time = new DcmTime(_h, _mn, _s, _us);
  TimeZone tz = _tz;
}
//TODO or we could use milliseconds since the Unix epoch, but we would lose reduced
//     precission dates, times and dateTimes
/**
 * The number of milliseconds since the "Unix epoch" 1970-01-01T00:00:00Z (UTC).
 *
 * This value is independent of the time zone.
 *
 * This value is at most 8,640,000,000,000,000ms (100,000,000 days) from the Unix epoch.
 * In other words: [:millisecondsSinceEpoch.abs() <= 8640000000000000:].
 *
 */
  final int millisecondsSinceEpoch;
*/
class DateTime {
  int nullValues;
  int _y, _m, _d, _h, _mn, _s, _us;
  TimeZone _tz;
  String   _dMark = '.';  // Decimal mark

  /// Constructs a [DateTime] object.  All unsupplied arguments default to null.
  DateTime(this._y,[this._m, this._d, this._h, this._mn, this._s, this._us, this._tz]);

  //TODO determine if there is any way that a "truncated" [DateTime] can occur?
  // Getters
  int      get year        => _y;
  int      get month       => _m;
  int      get day         => _d;
  int      get hour        => _h;
  int      get minute      => _mn;
  int      get second      => _s;
  int      get microsecond => _us;
  TimeZone get timeZone    => _tz;
  String   get _yStr       => dtu.yStr(_y);
  String   get _mStr       => dtu.mStr(_m);
  String   get _dStr       => dtu.dStr(_d);
  String   get _hStr       => dtu.hStr(_h);
  String   get _mnStr      => dtu.mnStr(_mn);
  String   get _sStr       => dtu.sStr(_s);
  String   get _usStr      => dtu.usStr(_dMark, _us);
  String   get _tzStr      => dtu.tzStr(_tz);
  String   get httpString  =>
      '$_yStr-$_mStr-$_dStr' + 'T' + '$_hStr:$_mnStr:$_sStr$_usStr$_tzStr';
  String   get dicomString   =>
      '$_yStr$_mStr$_dStr$_hStr$_mnStr$_sStr$_usStr$_tzStr';

  DateTime bytesToUtcDateTime(Uint8List date, Uint8List time, Uint8List tz) {
    final Uint8List _bytes = new Uint8List(19);
    DT dt = new DT.forDateTime();
    for (int i = 0; i < 19; i++) {
      int dlen = date.length;
      for(int j = 0; j < dlen; j++) _bytes[j] = date[j];
      int tlen = time.length;
      for(int j = 0; j < tlen; j++) _bytes[dlen+j] = time[j];
      int tzStart = dlen + tlen;
      for(int j = 0; j < tz.length; j++) _bytes[tzStart+j] = tz[j];
    }
    var dtr = new DateTimeReader(_bytes);
    return dtr.readDateTime;
  }

  DateTime utcDateTime(Date date, Time time, TimeZone tz) {
    return new DateTime(date.year, date.month, date.day,
                        time.hour, time.minute, time.second, time.microsecond,
                        tz);

  }

  /*
  //TODO define decimalSeparator as ".", "," or some other char.
  //     and define SPACE = " ".
  /// Parse a valid DICOM date.
  static DcmDateTime parseString(String str, [start = 0, end]) {
    int y, m, d, h, mm, s, us, sign, zz, xx;
    if (end == null) end = str.length;
    int len = end - start;
    if (len > 26) _error('DateTime string=$str too long');
    if (len < 4) _error('DateTime string=$str too short');

    DateTimeReader buf = new DateTimeReader.fromString(str, start, start, len);
    if (len >= 4)   y = buf.stringToInt(4, firstYear, lastYear);
    if (len >= 6)   m = buf.stringToInt(2, 1, 12);
    if (len >= 8)   d = buf.stringToInt(2, 1, DcmDate.daysInMonth(y, m));
    if (len >= 10)  h = buf.stringToInt(2, 0, 23);
    if (len >= 12) mm = buf.stringToInt(2, 0, 59);
    if (len >= 14)  s = buf.stringToInt(2, 0, 60);
    if (len > 14) {
      int i = start + 14;
      String char = str[i];
      if ((char == ".") || (char == ",")) {
        us = stringToInt(str, 15, end, 0, 999999);
      } else if (char == " ") {
        if (isTrailingSpace(str, 15, end)) return
      } else {
        if (char == '+') sign = 1;
        else if (char == '-') sign = -1;
        else
      }

    }
    return new DcmDateTime(y, m, d, h, mm, s, us);

  }

  static bool isTrailingSpace(String s, int start, int end) {
    int i = start;
    for(i; i < end; i++) if (s[i].codeUnitAt(0) != Ascii.SPACE) {
      _error('illegal embedded space found');
    }
    return true;
  }

  // Read a positive integer (radix 10) of exact length nChars.
  // Trailing space is allowed, but leading and embedded space is illegal.
  static int stringToInt(String s, int start, int end, int min, int max) {
    int n = 0;
    for(int i = start; i < end; i++) {
      int char = s[i].codeUnitAt(0);
      if ((char >= Ascii.DIGIT_0) && (char <= Ascii.DIGIT_9)) {
        n = (n * 10) + (char - Ascii.DIGIT_0);
      } else if (char == Ascii.SPACE) {
        if (isTrailingSpace(s, i++, end)) return (i == start) ? null : n;
      } else _error('invalid character= "${char.toString()}"');
    }
    if ((n < min) || (n > max)) _error('value out of range');
    return n;
  }

  /**
   * Parse a Uint8List into a valid DICOM date, and return it.
   * The maximum length of the string is 16. The string may be padded with trailing
   * spaces. Leading and embedded spaces are not allowed.
   */
  static DcmTime parseBytes(Uint8List bytes, [int start=0, end]) {
    int h, m, s, us;
    if (end == null) end = bytes.length;
    if ((end - start) > 16) _error('Time string=$bytes too long');
    int len = end - start;
    if (len >= 2) h = bytesToInt(bytes, start, start + 2, 0, 23);
    if (len >= 4) m = bytesToInt(bytes, start + 2, start + 4, 0, 59);
    if (len >= 6) s = bytesToInt(bytes, start + 4, start + 6, 0, 60);
    if (len > 6) {
      int i = start + 6;
      print('dec point = ${bytes[i]}');
      if ((bytes[i] != Ascii.PERIOD) || (bytes[i] != Ascii.COMMA))
        _error('invalid character where decimal indicator should be');
      us = bytesToInt(bytes, start + 7, end, 0, 999999);
    }
    return new DcmTime(h, m, s, us);
    //} else {
    //  return _error('Invalid DateDcm:$bytes');
  }

  /// Parse a positive integer (radix 10) of exact length = end - start,
  /// and return its value.
  static int bytesToInt(Uint8List bytes, int start, int end, int min, int max) {
    //TODO when debugged put this in DcmDate
    if (bytes[start] == Ascii.SPACE) {
      for(int i = start+1; i < end; i++) {
        if (bytes[i] != Ascii.SPACE) _error('illegal embedded space found');
      }
    }
    int n = 0;
    for(int i = start; i < end; i++) {
      int char = bytes[i];
      if ((char >= Ascii.DIGIT_0) && (char <= Ascii.DIGIT_9)) {
        n = (n * 10) + (char - Ascii.DIGIT_0);
      } else if (char == Ascii.SPACE) {
        _error('invalid whitespace');
      } else _error('invalid CodeUnit= $char');
    }
    //Note: this test is duplicated in the constructor.
    if ((n < min) || (n > max)) _error('value out of range');
    return n;
  }
 */

  void debug() {
    print('DateTime: y=$_y, m=$_m, d=$_d, h=$_h, m=$_m, s=$_s, us=$_us, tz=$_tz]');
    print('\tB=$dicomString, E=$httpString');
  }

  toString() => httpString;

  DateTime _error(msg) => throw new ArgumentError(msg);
}