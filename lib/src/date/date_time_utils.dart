// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library dcm_date_time_utils;

import 'package:utilities/utilities.dart';
import 'package:core/src/date/time_zone.dart';

// TODO cleanup and document
/**
 * Utility functions for dates and times
 *
 * Note: this package has not been tested for negative dates, times, or dateTimes.
 */

/// Year Utilities
const int MIN_YEAR  = 0000;
const int MAX_YEAR  = 9999;
      int firstYear = 1582; // As specified in ISO 8601:2000
      int lastYear  = 9999;

/// Sets the valid year range from [first] to [last] inclusive.
void setYearRange(int first, int last) {
  if (((first >= MIN_YEAR) && (first <= MAX_YEAR))
      && ((last > first) && (last <=MAX_YEAR))) {
    firstYear = first;
    lastYear = last;
  } else {
    error('Invalid Year Range $first:$last.'
        'The minimum year is 0000 and the maximum year is 9999');
  }
}

/// Returns [true] if [yesr] is in the range [firstYear] to [lastYear]
/// inclusive; otherwise, throws an error.
bool isValidYear(int year) {
  if ((year < firstYear) || (year > lastYear)) error('Invalid Year:$year');
  return true;
}

/// Returns [true] if the [year] and [month] are valid; otherwise, throws an error.
const int JANUARY  = 01;
const int DECEMBER = 12;
bool isValidMonth(int year, int month) {
  isValidYear(year);
  if ((month < DateTime.JANUARY) || (month > DateTime.DECEMBER))
    error('Invalid Month:$month');
  return true;
}

/// Returns true if the [year], [month], and [day], are valid ; otherwise, throws an error.
bool isValidDay(int year, int month, int day) {
  if ( !isValidMonth(year, month) &&
     ((day < 1) || (day > daysInMonth(year, month))))
     error('Invalid Month:$day');
  return true;
}

/// Returns [true] if [c] is a valid TimeMark.
bool isValidTimeMark(List chars, int c) => (chars.contains(c)) ? true : false;

// Returns the [hour] if it is valid; otherwise throws an error.
 bool isValidHour(int hour) {
  if ((hour == null) || (hour < 0) || (hour > 23)) error('Invalid Hour:$hour');
  return true;
}

// Returns the in
bool isValidMinute(int hour, int minute) {
  isValidHour(hour);
  if ((minute == null) || ((minute < 0) || (minute > 59)))
    error('Invalid Minute:$minute');
  return true;
}

bool isValidSecond(int hour, int minute, int second) {
  isValidMinute(hour, minute);
  if ((second == null) || ((second < 0) || (second > 60)))
  error('Invalid Second:$second');
  return true;
}

bool isValidDecimalMark(int char) => (char == $_PERIOD) || (char == $_COMMA);

bool isValidMicrosecond(int hour, int minute, int second, int microsecond) {
  isValidSecond(hour, minute, second);
  if ((microsecond != null) && ((microsecond < 0) || (microsecond > 999999)))
  error('Invalid Microsecond:$microsecond');
  return true;
}

bool isValidTimeZone(int sign, int hours, int minutes) {
  if ((sign != 1) && (sign != -1)) error('TimeZone offset must have a sign (+/-)');
  if (((sign == 1) && (hours > 12)) || ((sign == -1) && (hours < -14)))
      error('invalid hour=$hours');
  //TODO add checks for the set of minutes used in probability order
  isValidMinute(hours, minutes);
  return true;
}

/// Returns the number of days in the [month] of [year].  Primarily needed
/// for computing the number of days in February
const FEBRUARY = 2;
int daysInMonth(int year, int month) {
  const month_days = const [-1, 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  isValidMonth(year, month);
  int days = month_days[month];
  if ( days > 0) return days;
  if (month != FEBRUARY) error('invalid month number: $month');
  return  isLeapYear(year) ? 29 : 28;
}

/**
 * Returns [true] if the [year] is a leap year, [false] otherwise.
 *
 * The algorithm is:
 *    if year is not divisible by 4 then common year
 *    else if year is not divisible by 100 then leap year
 *    else if year is not divisible by 400 then common year
 *    else leap year
 *
 * See:
 *   <http://en.wikipedia.org/wiki/Leap_year>
 *   <http://www.timeanddate.com/date/leapyear.html>
 */
/* second is simpler
bool isLeapYear(int year) {
  if ((year % 4) != 0) return false;
  else if ((year % 100) != 0) return true;
  else if ((year % 400) != 0) return false;
  else return true;
}
*/
bool isLeapYear(int year) => (year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0));

int monthDay(int year, int month) {
  const commonYear = const [-1, 0, 31,  59,  90,  120, 151, 181, 212, 243, 273, 304, 334];
  const leapYear   = const [-1, 0, 31,  60,  91,  121, 152, 182, 213, 244, 274, 305, 335];
  return (isLeapYear(year)) ? commonYear[month] : leapYear[month];
}

int ordinalDate(int year, int month, int day) => monthDay(year, month) + day;

//TODO decide if needed
/*
final List months = ["January", "February", "March", "April", "May", "June", "July",
                     "August", "September", "October", "November", "December"];
//TODO needed?
String numberToMonth(int n) {
  if ((n < 1) || (n > 12)) throw new ArgumentError("invalid month number");
  else return months[n-1];
  }
*/
/*
//TODO Decide if  this is needed/useful? isn't Dart/core DateTime.JANUARY enought?
class Month {
  final Symbol name;
  final int number;

  const Month(this.name, this.number);

  static const JANUARY = const Month(#January, 1);
  static const FEBRUARY = const Month(#February, 2);
  static const MARCH = const Month(#March, 3);
  static const APRIL = const Month(#April, 4);
  static const MAY = const Month(#May, 5);
  static const JUNE = const Month(#June, 6);
  static const JULY = const Month(#July, 7);
  static const AUGUST = const Month(#August, 8);
  static const SEPTEMBER = const Month(#September, 9);
  static const OCTOBER = const Month(#October, 10);
  static const NOVEMBER = const Month(#November, 11);
  static const DECEMBER = const Month(#December, 12);
}
*/

String yStr(int y)   => '${_zeroPad(2, y)}';
String mStr(int m)   => (m == null)  ? '' : '${_zeroPad(2, m)}';
String dStr(int d)   => (d == null)  ? '' : '${_zeroPad(2, d)}';
String hStr(int h)   => (h == null)  ? '' : '${_zeroPad(2, h)}';
String mnStr(int m)  => (m == null)  ? '' : '${_zeroPad(2, m)}';
String sStr(int s)   => (s == null)  ? '' : '${_zeroPad(2, s)}';
String usStr(String m, int us) => (us == null) ? '' : '$m$us';
String tzStr(TimeZone tz)      => (tz == null) ? '' : '${tz.dicomString}';
String etzStr(TimeZone tz)     => (tz == null) ? '' : '${tz.httpString}';

//TODO move these to general utilities
String _zeroPad(int width, int n) {
  String s = n.toString();
  for(var i = s.length; i < width; i++) s = '0' + s;
  return s;
}

//TODO delete - replaced by Ascii.string in core/gen/Ascii.dart
//String charToString(int char) => '\"${ASCII.decode([char])}\"';

//TODO later for performance
/*
String ymStr(int y, int m, bool ext) => (ext) ? '$yStr-$mStr' : '$yStr$mStr';
String ymdStr(int y, int m, int d, bool ext) => (ext) ? '$yStr-$mStr-$dStr'
    : '$yStr$mStr$dStr';
String ymdhStr(int y, int m, int d, int h, bool ext) =>
    (ext) ? '$yStr-$mStr-$dStr' + 'T' + '$hStr' : '$yStr$mStr$dStr$hStr';
String ymdhmStr(int y, int m, int d, int h, int mn, bool ext) =>
    (ext) ? '$yStr-$mStr-$dStr' + 'T' + '$hStr:$mnStr' : '$yStr$mStr$dStr$hStr$mnStr';
String ymdhmsStr(int y, int m, int d, int h, int mn, int s, bool ext) =>
    (ext) ? '$yStr-$mStr-$dStr' + 'T' + '$hStr:$mnStr:$sStr'
          : '$yStr$mStr$dStr$hStr$mnStr';
String ymdhmsuStr(int y, int m, int d, int h, int mn, int s, int us, bool ext) =>
    (ext) ? '$yStr-$mStr-$dStr' + 'T' + '$hStr:$mnStr:$sStr.$usStr'
          : '$yStr$mStr$dStr$hStr$mnStr.$usStr';
String ymdhmsuhStr(int y, int m, int d, int h, int mn, int s, int us, bool ext) =>
    (ext) ? '$yStr-$mStr-$dStr' + 'T' + '$hStr:$mnStr:$sStr.$usStr'
          : '$yStr$mStr$dStr$hStr$mnStr.$usStr';

/*
String formatYearMonthDay(int year, int month, int day) =>
  formatYearMonth(year, month) + '-' + zeroPad(2, day);

String formatYearMonth(int year, int month) =>
  formatYear(year) + '-' + zeroPad(2, month);

String formatYear(int year) => zeroPad(4, year);

formatDateString(int year, int month, int day) {
  if (day != null) return formatYearMonthDay(year, month, day);
  else if (month != null) return formatYearMonth(year, month);
  else return formatYear(year);
}

String formatToMicrosecond(int hour, int minute, int second, int microsecond) =>
  formatToSecond(hour, minute, second) + "." + microsecond.toString();

String formatToSecond(int hour, int minute, int second) =>
  formatToMinute(hour, minute) + ':' + zeroPad(2, second);

String formatToMinute(int hour, int minute) =>
  formatToHour(hour) + ':' + zeroPad(2, minute);

String formatToHour(int hour) => zeroPad(2, hour);

formatTimeString(int hour, int minute, int second, int microsecond) {
  if (microsecond != null)
    return formatToMicrosecond(hour, minute, second, microsecond);
  else if (second != null)
    return formatToSecond(hour, minute, second);
  else if (minute != null) return formatToMinute(hour, minute);
  else return formatToHour(hour);
}

String formatDateTimeString(int year, int month, int day, int hour, int minute,
                                 int second, int microsecond, TimeZone timeZone) {
  String date = formatDateString(year, month, day);
  String time = formatTimeString(hour, minute, second, microsecond);
  return date + 'T' + time + TimeZone.httpString;
}
*/

// Reduced Precision support
class _nulls {
  static const int mBit    = 1;
  static const int dBit    = 2;
  static const int hBit    = 4;
  static const int mnBit   = 8;
  static const int sBit    = 16;
  static const int usBit   = 32;
  static const int tzhBit = 64;
  static const int tzmBit = 128;

  static int IN_DATE(m, d) {
    int result = 0;
    if (m == null) result = result | _nulls.mBit;
    if (m == null) result = result | _nulls.dBit;
    return result;
  }

  static int IN_TIME(mn, s, us, tzh, tzm) {
    int result = 0;
    if (mn  == null) result = result | _nulls.mnBit;
    if (s   == null) result = result | _nulls.sBit;
    if (us  == null) result = result | _nulls.usBit;
    if (tzh == null) result = result | _nulls.tzhBit;
    if (tzm == null) result = result | _nulls.tzmBit;
    return result;
  }

  static int IN_DATETIME(m, d, h, mn, s, us, tzh, tzm) {
    int result = 0;
    result = IN_DATE(m, d);
    if (h == null) result = result | _nulls.hBit;
    result = IN_TIME(mn, s, us, tzh, tzm);
    return result;
  }

  int FIRST_NULL(nullFields) {
    int field = 0, mask = 1, i = 1;
    while(i <= 8) {
      if ((nullFields & mask) != 1) break;
      mask *= 2;
      field++;
    }
    while(i <= 8) {
      if ((nullFields & mask) != 1) _error('invalid embedded null in Date, Time, or DateTIme');
    }
    return field;
  }
}
 */

bool printDebug = true;

void debug(String s) { if (printDebug) print(s); }

//Note: Throws an Exception rather than an Error.
Exception error(msg) => throw new FormatException(msg);
