// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library time_zone;

//TODO cleanup and document

import 'package:core/src/date/date_time_utils.dart' as dtu;

// Add information
class TimeZone {
  int      _inMinutes;
  String   _name = "Unknown TimeZone";

  //TODO add a lookupName(offset) function

  // Constructors
  TimeZone(int sign, int hours, int minutes, [this._name]) {
    dtu.isValidTimeZone(sign, hours, minutes);
    _inMinutes = ((sign * ( 60 * hours)) + (sign * minutes));
  }

  TimeZone.inMinutes(this._inMinutes, [this._name]);

  // Getters
  /// Returns the name of the [TimeZone].
  String get name => _name;

  /// Returns the offset in minutes from UTC for [this].
  int    get offsetInMintues => _inMinutes;
  int    get _s      => (_inMinutes < 0) ? -1 : 1; // sign
  int    get _h      => _inMinutes ~/ 60;          // hour
  int    get _m      => _inMinutes % 60;           // minute
  String get _tzSign   => (_s == 1) ? "+" : "-";   // sign as String
  String get _tzHour   => dtu.hStr(_h);            // hour as String
  String get _tzMinute => dtu.mStr(_m);            // minute as String


  String get offsetString  => '$_tzSign$_tzHour$_tzMinute';

  String format({extended: false}) {
    print('_inMinutes=$_inMinutes, _s=$_s, _h=$_h, _m=$_m, _tzSign=$_tzSign, _tzHour=$_tzHour, _tzMinute=$_tzMinute');
    if ((extended) && ((_s == 0) && (_h == 0) && (_m == 0))) return 'Z';
    return offsetString;
  }

  /// Returns the [TimeZone] of the local TimeZone where the program is executing.
  static TimeZone getLocalTimeZone() {
    DateTime dt = new DateTime.now();
    return new TimeZone.inMinutes(dt.timeZoneOffset.inMinutes, dt.timeZoneName);
    }

  String get dicomString => format(extended: false);  // DICOM format
  String get httpString  => format(extended: true);   // HTTP/1.1 format
  toString() => httpString;

  _error(msg) => throw new ArgumentError(msg);
}