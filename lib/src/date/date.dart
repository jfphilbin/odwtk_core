// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library date;

//TODO Cleanup and document

//TODO later for performance
//import 'package:core/date/dart_date_time.dart';
import 'package:core/src/date/date_time_utils.dart' as dtu;

/**
 *  This library implements and ISO 8601:2000 specifications.follows DICOM PS3.5
 *  The Minimum Year tested is 0000.  The default Minimum Year is 1582.
 *  The default Maximum Year is 9999 as specified in ISO 8601:2000
 *  The Minimum and Maximum year can be expanded by mutual agreement.
 */
class Date {
  //TODO later for performance
  //int firstNull;
  //DartDateTime date;
  int _y, _m, _d;

  // Constructor
  //TODO later for performance
  //Date(y, [m, d]) => new dart.DateTime(y, m, d);

  Date(this._y, [this._m, this._d]) {
    if (_d!= null) dtu.isValidDay(_y, _m, _d);
    else if (_m !=  null) dtu.isValidMonth(_y, _m);
    else dtu.isValidYear(_y);
  }

  // Getters
  int    get year  => _y;
  int    get month => _m;
  int    get day   => _d;
  bool   get isLeapYear => dtu.isLeapYear(_y);
  String get _yStr => dtu.yStr(_y);
  String get _mStr => dtu.mStr(_m);
  String get _dStr => dtu.dStr(_d);
  String get dicomString => '$_yStr$_mStr$_dStr';

  String get httpString {
    if ((_m == null) && (_d == null)) return '$_yStr';
    if (_d == null) return '$_yStr-$_mStr';
    return '$_yStr-$_mStr-$_dStr';
  }

  /*
  /// Parse a valid DICOM date. WARNING: No error checking!
  static DcmDate parseString(String str, [start = 0, end]) {
    int y, m, d;
    if (end == null) end = str.length;
    int len = end - start;
    if (len > 8) _error('Date string=$str too long');
    if ((len == 8) || (len == 6) || (len == 4)) {
      y = stringToInt(str, start, start + 4, firstYear, lastYear);
      if (len >= 6) m = stringToInt(str, start + 4, start + 6, 1, 12);
      if (len == 8) d = stringToInt(str, start + 6, start + 8, 1, daysInMonth(y, m));
      return new DcmDate(y, m, d);
    } else {
      return _error('Invalid DateDcm:$str');
    }
  }

  // Read a positive integer (radix 10) of exact length nChars.
  static int stringToInt(String s, int start, int end, int min, int max) {
    int n = 0;
    for(int i = start; i < end; i++) {
      int char = s[i].codeUnitAt(0);
      if ((char >= Ascii.DIGIT_0) && (char <= Ascii.DIGIT_9)) {
        n = (n * 10) + (char - Ascii.DIGIT_0);
      } else _error('invalid CodeUnit= $char');
    }
    if ((n < min) || (n > max)) _error('value out of range');
    return n;
  }

  /// Parse a Uint8List into a valid DICOM date, and return it.
  static DcmDate parseBytes(Uint8List bytes, [int start=0, end]) {
    int y, m, d;
    if (end == null) end = bytes.length;
    int len = end - start;
    if (len > 8) _error('Date string=$str too long');
    if ((len == 8) || (len == 6) || (len == 4)) {
      y = bytesToInt(bytes, start, start + 4, firstYear, lastYear);
      if (len >= 6) m = bytesToInt(bytes, start + 4, start + 6, 1, 12);
      if (len == 8) d = bytesToInt(bytes, start + 6, start + 8, 1, daysInMonth(y, m));
      return new DcmDate(y, m, d);
    } else {
      return _error('Invalid DateDcm:${bytes.toString()}');
    }
  }

  /// Parse a positive integer (radix 10) of exact length = end - start,
  /// and return its value.
  static int bytesToInt(Uint8List bytes, int start, int end, int min, int max) {
    int n = 0;
    for(int i = start; i < end; i++) {
      int char = bytes[i];
      if ((char >= Ascii.DIGIT_0) && (char <= Ascii.DIGIT_9)) {
        n = (n * 10) + (char - Ascii.DIGIT_0);
      } else if (char == Ascii.SPACE) {
        _error('invalid whitespace');
      } else _error('invalid CodeUnit= $char');
    }
    //Note: this test is duplicated in the constructor.
    if ((n < min) || (n > max)) _error('value out of range');
    return n;
  }
  */

  void debug() {
    print('Date: [y=$_y, m=$_m, d=$_d]');
    print('\tB=\"$dicomString\", E=\"$httpString\"');
  }

  toString() => httpString;

  _error(msg) => throw new FormatException(msg);
}