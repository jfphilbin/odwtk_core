
 
The grammar defines a profile of ISO 8601:2000(E)
which includes the subset accepted by IETF RFC 3339.
The syntax of the grammar conforms to IETF RFC 5234.

This profile supports Dates, Times, and DateTimes.
This profile places the following restrictions on ISO 8601:2000(E) 
  - Years MUST have at least four digits
  
The DateParser by default supports only 
  - years from 1582 to 9999, this can be extended to 
    -99999 to 99999 using the startYear and endYear parameters.
    If the startYear - endYear range does not include negative years
    then the year component can not be prefixed with a sign [+/-].
    
  

It supports Date, Time, and DateTime objects.

H1 Dates H1

Dates can be in either Basic format, for example "19720721" or Extended format, 
for example "1972-07-21".  Dates may have *reduced precision*, e.g. "1950" or "1950-10",
but they may not be *truncated*, e.g. "-720721" or "-0721". Dates may have expanded 
representations, for example "+019720721".

Examples of accepted Date strings:
  "19720721"
  "1972-07-21"
  "197207"
  "1972-07"
  "1972"
  
  
The following is the grammar, in [ABNF], for Dates:

## Date Grammar##
    date              = basicDate / exDate
  
    basicDate         = posYear month_opt
    month_opt         = <empty> / month day_opt
    day_opt           = <empty> / day
  
    exDate            = year exMonth_opt
    exMonth_opt       = <empty> / '_' month exDay_opt
    exDay_opt         = <empty> / '-' day

    year              = sign_opt posYear
    sign_opt          = <empty> / sign
    posYear           = 4DIGIT  ; min=minYear, max=maxYear }
    month             = 2DIGIT  ; min=01, max=12 }
    day               = 2DIGIT  ; min=01, max=daysInMonth(year, month) }
 
    Note: daysInMonth is a function that returns the number of days
          in year-month.  In particulatr, it computed the correct number
          of days for the month of February based on whether it is a leap
          year or not.
          
Times

Times use a 24 hour clock with hours expressed using 2 digit from 00 to 23.
This profile does not allow the use of hour 24.  Times can be in either 
Basic format, for example "140205" or Extended format, 
for example "14:02:05".  Times may have *reduced precision*, e.g. "14:" or "1402",
but they may not be *truncated*, e.g. "-0205" or "--21". The Extended format for 
Time may be prefixed with the time designator "T".  The second component of a Time 
may include a decimal fraction, which is appended to the integer part of the second.
The decimal fraction of a second includes the decimal sign, either "," or "." as
specified in ISO 31-0.  Times may have have a suffix that is either the Coordinated
Universal Time (UTC) designator "Z" or "z" or a local timezone offset from UTC.

Time Basic Format
    - not separators

    ?? should it include the time designator 
    ?? should it allow the UTC designator
    ?? should it allow timezone offsets in hours without minutes?
    
Time Extended Format
    - MUST include time designator "T"
    - MUST include the ":" separator between hour and minute and minute and second
    - If timezone included it MUST use the Extended format for timezones.
    
This profile does not support:
    - The integer 24 in the hours component.  In particular, midnight can only 
      be represented by "000000" or "00:00:00" and NOT by "240000" or "24:00:00"     
    - Decimal fractions for the hours or minutes components
    - Truncated representations, e.g. T-0205 or T-02-05
    - ??Use of the time designator in Basic format
    - local timezone offsets that include only hours.  Local timezone offsets
      MUST include both hours and minutes.

## Time Grammar ##

    time             = basicTime / exTime
  
    basicTime        = hour minute_opt basicTimezone_opt
    minute_opt       = <empty> / minute second_opt
    second_opt       = <empty> / second micro_opt
  
    exTime           = timeMark hour exMinute_opt exTimezone_opt
    timeMark         = 'T' / 't' /  SPACE
    exMinute_opt     = <empty> / ':' minute exSecond_opt
    exSecond_opt     = <empty> / ':' second micro_opt

have Timezones.
The result is always in either local time or UTC.
If a time zone offset other than UTC is specified,
the time is converted to the equivalent UTC time.

Examples of accepted strings:
  "2012-02-27 13:27:00"
  "2012-02-27 13:27:00.123456z"
  "20120227 13:27:00"`
  "20120227T132700"
  "20120227"
  "+20120227"
  "2012-02-27T14Z"
  "2012-02-27T14+00:00"
  "-123450101 00:00:00 Z"    : in the year -12345.
  "2002-02-27T14:00:00-0500" : Same as `"2002-02-27T19:00:00Z"`



  parameters:
    bool positiveYearsOnly
    int  minYear         = 1582;      // defaultMinYear
    int  maxYear         = 9999;      // defaultMaxYear
    String dateMark      = '-';       // default Date separator mark
    String timeMark      = ':';       // default Time separator mark
    String dateTimeMark  = 'T';       // default DateTime separator mark
    String decimalMark   = ',' | '.'; // default Decimal point mark
    String thousandsMark = ',';       // default Thousands separator mark
    int    daysInMonth(year, month)   // returns last day in specified month
    int    maxSeconds(year, month, day, hour, minute) // 59, or 60 if leap second

  ## Date Terms##
  
    date              = basicDate / exDate
  
    basicDate         = posYear month_opt basicTimezone_opt
    month_opt         = <empty> / month day_opt
    day_opt           = <empty> / day
  
    exDate            = year exMonth_opt exTimezone_opt
    exMonth_opt       = <empty> / '_' month exDay_opt
    exDay_opt         = <empty> / '-' day
 
## Time Terms ##

    time             = basicTime / exTime
  
    basicTime        = hour minute_opt basicTimezone_opt
    minute_opt       = <empty> / minute second_opt
    second_opt       = <empty> / second micro_opt
  
    exTime           = timeMark_opt hour exMinute_opt exTimezone_opt
    timeMark_opt     = <empty> / timeMark
    timeMark         = 'T' / 't' /  SPACE
    exMinute_opt     = <empty> / ':' minute exSecond_opt
    exSecond_opt     = <empty> / ':' second micro_opt

## DateTime Terms ##

    dateTime         = basicDateTime / exDateTime

    basicDateTime    = basicDate time_opt basicTimezone_opt
    time_opt         = <empty> / basicTime
      
    exDateTime       = exDate exTime_opt exTimezone_opt
    exTime_opt       = <empty> / exTime

## Timezone Terms##
    basicTimezone_opt = <empty> /sign hour minute
    
    exTimezone_opt   = space_opt exTimezone
    exTimezone       = 'Z' / 'z' / sign hours timezonemins_opt
    timezonemins_opt = <empty> / ':' minutes / minutes

## General Terms ##

    year             = sign_opt posYear
    sign_opt         = <empty> / sign
    posYear          = 4DIGIT   ; min=minYear, max=maxYear - See "Years Range" above
    month            = 2DIGIT   ; min=01, max=12
    day              = 2DIGIT   ; min=01, max=31           - See "Days In Month" section above
    hour             = 2DIGIT   ; min=00, max=23
    minute           = 2DIGIT   ; min=00, max=59
    second           = 2DIGIT   ; min=00, max=60           - See "Max Second" secont above
    micro_opt        = <empty> / '.' 1*6DIGIT  
    sign             = '+' / '-'
    space_opt        = <empty> / ' '
    <empty>          = indicates a term with no content