// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library sequence;

import 'dart:collection';

import 'package:core/dataset.dart';
import 'package:dictionary/vr.dart';

/*
 * A DICOM Sequence (VR=SQ)
 *
 * [This] is the base class for [Sequence]s.  Every [Sequence] is an [Attribute] with
 * a [Tag], a [VR] = [VR.SQ], and a [Value] that is a [List] of DICOM [Items], where
 * each [Item] is a [Datasets].
 */
class Sequence extends ListBase<Dataset> {
  final int           tag;       // The Tag that identifies this attribute
  final VR            vr;        // All Sequences have a VR = 'SQ'
  final Dataset       parent;    // The Dataset that contains this Sequence
  final List<Dataset> items;     // Each [Item] is a Dataset.
  final Uri           docLink = Uri.parse('doc/dicom/dart/sequence.html');

  // Getters & Setters
  List<Dataset> get value => items;

  // Constructor
  /**
   * A DICOM [Sequence] [Attribute].
   *
   *
   */
  Sequence(this.tag, this.vr, this.parent, this.items);

 /*
  * The basic map primitives to make the MixIn work
  *
  * All operations are defined in terms of `length`, `operator[]`,
  * `operator[]=` and `length=`, which need to be implemented.
  */
  int  get length => items.length;
  void set length(int l) { items.length = l; }

  Dataset operator [](int i) => items[i];
  void    operator []=(int i, Dataset ds) { items[i] = ds; }

}
