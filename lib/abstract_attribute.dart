// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library attribute;

import 'package:dictionary/dictionary.dart';

/**
 * An abstract DICOM [Attribute].
 */
abstract class Attribute {


  int     get tag;
  String  get tagAsHex => tagToHex(tag);
  VR      get vr;
  dynamic get value;

  Attribute(tag, vr, value);

  bool operator == (Attribute a) {
    if ((a.tag != tag) || (a.vr != vr) || (a.value != value)) return false;
    else return true;
  }

  bool operator < (Attribute a) => (tag < a.tag) ? true : false;

  bool operator > (Attribute a) => (tag > a.tag) ? true : false;




  String toString() => "Attribute[tag=${tagToHex(tag)}, vr=${vr.name}, value=$value]";
}
