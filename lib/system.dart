// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library system;

/// Global Variables
class System {
  static bool DEBUG = true;
  static int  logLevel = 0;

  static DateTime StartTime = new DateTime.now();
  static String localTimezoneName = StartTime.timeZoneName;
  static Duration localTimezoneOffset = StartTime.timeZoneOffset;
}