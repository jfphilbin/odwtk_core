// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library toolkit_version;

/**
 * [this] contains the information that should go into the DICOM
 * File Meta Imformation for Part 10 files created by the Open DICOMweb Toolkit.
 */
class Version {

  static const String number = "0.1.1";
  static const String name = "Open DICOMweb Toolkit $number";

  //TODO get a UID for this implementation
  static const String classUID = "";  // Currently no UID.

}
