// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library dicom_options;

/// An enumerated set that specifies options when generating a dump of a DICOM object.
class DicomDumpOption {
  final String name;
  final int    value;

  const DicomDumpOption(this.name, this.value);

  static const None =
      const DicomDumpOption("None", 0);
  static const ShortenLongValues =
      const DicomDumpOption("ShortenLongValues", 1);
  static const Restrict80CharactersPerLine =
      const DicomDumpOption("Restrict80CharactersPerLine", 2);
  static const KeepGroupLengthElements =
      const DicomDumpOption("KeepGroupLengthElements", 4);

  //Note: Default = ShortenLongValues | Restrict80CharactersPerLine = 5
  static const Default = const DicomDumpOption("Default", 5);

  static bool isSet(DicomDumpOption option, DicomDumpOption flag) {
    return (option.value & flag.value) == flag.value;
  }
}

/// /// An enumerated set that specifies options when reading a DICOM object.
class DicomReadOption {
  final String name;
  final int    value;

  const DicomReadOption(this.name, this.value);

  static const None =
      const DicomReadOption("None", 0);
  static const KeepGroupLengths =
      const DicomReadOption("KeepGroupLengths", 1);
  static const UseDictionaryForExplicitUN =
      const DicomReadOption("UseDictionaryForExplicitUN", 2);
  static const AllowSeekingForContext =
      const DicomReadOption("AllowSeekingForContext", 4);
  static const ReadNonPart10Files =
      const DicomReadOption("ReadNonPart10Files", 8);
  static const DoNotStorePixelDataInDataSet =
      const DicomReadOption("DoNotStorePixelDataInDataSet", 16);
  static const StorePixelDataReferences =
      const DicomReadOption("StorePixelDataReferences", 32);

  //Note: Default = UseDictionaryForExplicitUN | AllowSeekingForContext | ReadNonPart10Files = 14
  static const Default = const DicomReadOption("Default", 14);

  static bool isSet(DicomReadOption option, DicomReadOption flag) {
    return (option.value & flag.value) == flag.value;
  }
}


/// An enumerated set that specifies options when writing a DICOM object.
class DicomWriteOption {
  final String name;
  final int    value;

  const DicomWriteOption(this.name, this.value);

  static const None =
      const DicomWriteOption("None", 0);
  static const CalculateGroupLengths =
      const DicomWriteOption("CalculateGroupLengths", 1);
  static const ExplicitLengthSequence =
      const DicomWriteOption("ExplicitLengthSequence", 2);
  static const ExplicitLengthSequenceItem =
      const DicomWriteOption("ExplicitLengthSequenceItem", 4);
  static const WriteFragmentOffsetTable =
      const DicomWriteOption("WriteFragmentOffsetTable", 8);
  static const DoNotStorePixelDataInDataSet =
      const DicomWriteOption("DoNotStorePixelDataInDataSet", 16);
  static const StorePixelDataReferences =
      const DicomWriteOption("StorePixelDataReferences", 32);

  //Note: Default = WriteFragmentOffsetTable
  static const Default = const DicomWriteOption("Default", 8);

  static bool isSet(DicomWriteOption option, DicomWriteOption flag) {
    return (option.value & flag.value) == flag.value;
  }
}