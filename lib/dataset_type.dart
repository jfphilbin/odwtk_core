// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library dataset_type;

/*
 * Defines the differnt types of DICOM datasets.
 *
 *
 */
//TODO change to Enum DStype {top, patient, study, series, instance, item};
//     when Enum is available.
class DSType {
  final String name;
  final int    index;
  final String docLink = "doc/dicom/dataset_type.html";

  const DSType(this.name, this.index);

  /// A [Dataset] containing only [FileMetaInfo] attributes.
  static const FILE_META_INFO = const DSType("FileMetaInfo", 0);

  // The top level SFD Dataset
  static const TOP             = const DSType("Top", 1);

  // The top level MSD Dataset
  static const PATIENT_STUDIES = const DSType("PatientStudies", 2);

  // MSD Dataset containing Patient Attributes
  static const PATIENT         = const DSType("Patient", 3);

  // MSD Dataset containing Study Attributes
  static const STUDY           = const DSType("Study", 4);

  // MSD Dataset containing Series Attributes
  static const SERIES          = const DSType("Series", 5);

  // MSD Dataset containing Instance Attributes
  static const INSTANCE        = const DSType("Instance", 6);

  // SFD or MSD Dataset that is an Item in  a Sequence
  static const ITEM            = const DSType("Item", 7);
  //static const top = const DSType("Top", 0);

  bool operator ==(other) => name == other.name;
  bool operator <(other)  => index < other.index;
  bool operator >(other)  => index > other.index;

  String toString() => name;
}
