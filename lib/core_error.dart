// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library core_error;
/**
 * The DICOM parser has arrived at an inconsistent state and cannot proceed.
 *
 * This is a generic error used for a variety of different erroneous
 * actions. The message should be descriptive.
 */
class DicomParseError extends Error {
  final String message;

  DicomParseError([this.message]);

  String toString() {
    if (message != null)
      return "DICOM Parse Error: $message";
    else
      return "Dicom Parse Error";
  }
}

void parseError(String message) {
  throw new DicomParseError(message);
}

class DicomError extends Error {
  final message;

  DicomError([this.message]);

  String toString() {
    if (message != null)
      return "DICOM error: $message";
    else
      return "DICOM error";
  }
}

//TODO replace general errors with more specific ones for version 0.2.0
/**
 * General error message
 *
 */
void error(message) {
  throw new DicomError(message);
}

/**
 * RangeErrors indicating an invalid index, length or value into ByteBuf.
 */
void rangeError(String msg) {
  throw new RangeError(msg);
}
